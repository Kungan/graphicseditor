﻿namespace WpfGraphicsEditor
{
	/// <summary>
	/// Представляет собой градиенты для отдельного пикселя.
	/// </summary>
	public struct PixelGradient
	{
		/// <summary>
		/// Красная составляюая пикселя.
		/// </summary>
		public byte R
		{
			get;
			set;
		}

		/// <summary>
		/// Зеленая составляющая пикселя.
		/// </summary>
		public byte G
		{
			get;
			set;
		}

		/// <summary>
		/// Синяя составляющая пикселя.
		/// </summary>
		public byte B
		{
			get;
			set;
		}

		/// <summary>
		/// Массив градиентов.
		/// </summary>
		public float[] Gradients
		{
			get;
			set;
		}
	}
}
