﻿using PostSharp.Aspects;
using PostSharp.Aspects.Advices;
using PostSharp.Extensibility;
using PostSharp.Reflection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfGraphicsEditor
{
	/// <summary>
	/// Аттрибут для реализации интерфейса NotifyPropertyChanged путем инъекции в IL код при помощи PostSharp.
	/// </summary>
	[Serializable]
	[IntroduceInterface(
		typeof(INotifyPropertyChanged),
		OverrideAction = InterfaceOverrideAction.Ignore)]
	[MulticastAttributeUsage(
		MulticastTargets.Class,
		Inheritance = MulticastInheritance.Strict)]
	public sealed class NotifyPropertyChangedAttribute : InstanceLevelAspect, INotifyPropertyChanged
	{
		/// <summary>
		/// Вызывает событие, уведомляющее об изменении свойства.
		/// </summary>
		/// <param name="propertyName">Имя свойства.</param>
		[IntroduceMember(
			Visibility = Visibility.Family,
			IsVirtual = true,
			OverrideAction = MemberOverrideAction.OverrideOrFail)]
		public void OnPropertyChanged(string propertyName)
		{
			PropertyChanged?.Invoke(Instance, new PropertyChangedEventArgs(propertyName));
		}

		/// <summary>
		/// Событие, происходящие при изменении любого свойства.
		/// </summary>
		[IntroduceMember(OverrideAction = MemberOverrideAction.OverrideOrFail)]
		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Происходит при установке любого свойства.
		/// </summary>
		[OnLocationSetValueAdvice, MulticastPointcut(
			Targets = MulticastTargets.Property,
			Attributes = MulticastAttributes.Instance)]
		public void OnPropertySet(LocationInterceptionArgs args)
		{
			object currentValue = args.GetCurrentValue();
			if (args.Value == args.GetCurrentValue()) return;

			args.ProceedSetValue();
			OnPropertyChanged(args.Location.Name);
		}

		/// <summary>
		/// Происходит при вызове RasePropertyChanged внутри класса.
		/// </summary>
		[OnMethodEntryAdvice]
		[MulticastPointcut(
			Targets = MulticastTargets.Method,
			MemberName = "RasePropertyChanged",
			Attributes = MulticastAttributes.Instance)]
		public void OnEntry(MethodExecutionArgs eventArgs)
		{
			OnPropertyChanged(eventArgs.Arguments.First() as string);
		}


	}
}
