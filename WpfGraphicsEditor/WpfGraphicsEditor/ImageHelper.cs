﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using WpfGraphicsEditor.Model;

namespace WpfGraphicsEditor
{
	public static class ImageHelper
	{
		/// <summary>
		/// Разрешение по горизонтали для получаемого из контрола изображения.
		/// </summary>
		private const int dpi_X = 96;

		/// <summary>
		/// Разрешение по вертикали для получаемого из контрола изображения.
		/// </summary>
		private const int dpi_Y = 96;

		/// <summary>
		/// Сохраняет контрол как изображение.
		/// </summary>
		/// <param name="obj">Контрол</param>
		/// <returns>Изображение</returns>
		public static RenderTargetBitmap ToImageSource(FrameworkElement obj)
		{
			// Save current canvas transform
			Transform transform = obj.LayoutTransform;
			obj.LayoutTransform = null;
			// fix margin offset as well
			Thickness margin = obj.Margin;
			obj.Margin = new Thickness(
				0, 
				0,
				margin.Right - margin.Left,
				margin.Bottom - margin.Top
			);

			// Get the size of canvas
			Size size = new Size(obj.Width, obj.Height);

			// force control to Update
			obj.Measure(size);
			obj.Arrange(new Rect(size));

			RenderTargetBitmap bmp = new RenderTargetBitmap(
				(int)obj.Width, (int)obj.Height, dpi_X, dpi_Y, PixelFormats.Pbgra32);

			bmp.Render(obj);

			// return values as they were before
			obj.LayoutTransform = transform;
			obj.Margin = margin;
			return bmp;
		}

		/// <summary>
		/// Вычисляет поле градиентов.
		/// </summary>
		/// <param name="stride">Ширина изображения.</param>
		/// <param name="r">Красный канал.</param>
		/// <param name="g">Зеленый канал.</param>
		/// <param name="b">Синий канал.</param>
		/// <returns>Поле градиентов.</returns>
		/// <exception cref="Exception">Выбрасывается при разной длине массивов.</exception>
		/// <exception cref="Exception">Выбрасывается при длине массива не кратной ширине изображения.</exception>
		public static PixelGradient[,] CalculateGradients(int stride, byte[] r, byte[] g, byte[] b)
		{
			int length = r.Length;
			if (g.Length != length || b.Length != length)
			{
				throw new Exception("Разная длина массивов с цветовыми компонентами.");
			}

			if(length % stride !=  0)
			{
				throw new Exception("Неправильная длина строки.");
			}

			int height = r.Length / stride;
			PixelGradient[,] pixels = new PixelGradient[height, stride];

			int numNeighbor;
			int numCenter = 0;

			int[] num = new int[8];
			num[0] = numCenter - stride - 1;
			num[1] = numCenter - stride;
			num[2] = numCenter - stride + 1;
			num[3] = numCenter + 1;
			num[4] = numCenter + stride + 1;
			num[5] = numCenter + stride;
			num[6] = numCenter + stride - 1;
			num[7] = numCenter - 1;

			for(int h = 0; h < height; h++)
			{
				for(int w = 0; w < stride; w++)
				{
					PixelGradient pixel = new PixelGradient();
					pixel.R = r[numCenter];
					pixel.G = g[numCenter];
					pixel.B = b[numCenter];
					var gradients = new float[8];
					for (int i = 0; i < 8; i++)
					{
						numNeighbor = num[i];
						if ((i == 0 || i == 1 || i == 2) && h == 0 ||
							(i == 0 || i == 7 || i == 6) && w == 0 ||
							(i == 4 || i == 5 || i == 6) && h == height - 1 ||
							(i == 2 || i == 3 || i == 4) && w == stride - 1)
						{
							gradients[i] = 0;
						}
						else
						{
							gradients[i] = CalculateGradient(
								r[numCenter], r[numNeighbor],
								g[numCenter], g[numNeighbor],
								b[numCenter], b[numNeighbor]
							);
						}
						num[i]++;
					}
					pixel.Gradients = gradients;
					pixels[h, w] = pixel;
					numCenter++;
				}
			}
			return pixels;
		}

		/// <summary>
		/// Вычислает градиент между двумя пикселями.
		/// </summary>
		/// <param name="r1">Красный канал первого пикселя.</param>
		/// <param name="r2">Красный канал второго пикселя.</param>
		/// <param name="g1">Зеленый канал первого пикселя.</param>
		/// <param name="g2">Зеленый канал второго пикселя</param>
		/// <param name="b1">Синий канал первого пикселя.</param>
		/// <param name="b2">Синий канал второго пикселя.</param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static float CalculateGradient(byte r1, byte r2, byte g1, byte g2, byte b1, byte b2)
		{
			int rDiff = r1 - r2;
			int gDiff = g1 - g2;
			int bDiff = b1 - b2;
			return (float)Math.Sqrt(rDiff * rDiff + gDiff * gDiff + bDiff * bDiff);
		}

		/// <summary>
		/// Преобразует изображение в одномерный массив байт.
		/// </summary>
		/// <param name="bitmapSource">Изображение</param>
		/// <returns>Массив байт.</returns>
		public static byte[] BitmapSourceToArray(BitmapSource bitmapSource)
		{
			int stride = getStride(bitmapSource);
			byte[] pixels = new byte[bitmapSource.PixelHeight * stride];

			bitmapSource.CopyPixels(pixels, stride, 0);

			return pixels;
		}

		/// <summary>
		/// Рассчитывает количество байт на строку.
		/// </summary>
		/// <param name="bitmapSource">Изображение.</param>
		/// <returns>Количество байт на строку</returns>
		private static int getStride(BitmapSource bitmapSource)
		{
			return getStride(bitmapSource.PixelWidth, bitmapSource.Format);
		}

		/// <summary>
		/// Рассчитывает количество байт на строку.
		/// </summary>
		/// <param name="width">Ширина изображения в пикселях.</param>
		/// <param name="pixelFormat">Формат пикселях.</param>
		/// <returns>Количество байт на строку.</returns>
		private static int getStride(int width, PixelFormat pixelFormat)
		{
			// Stride = (width) x (bytes per pixel)
			return width * ((pixelFormat.BitsPerPixel + 7) / 8);
		}

		/// <summary>
		/// Создает изображение из массива пикселей.
		/// </summary>
		/// <param name="pixels">Массив пикселей.</param>
		/// <param name="width">Ширина изображения.</param>
		/// <param name="height">Высота изображения.</param>
		/// <returns>Изображение.</returns>
		public static BitmapSource BitmapSourceFromArray(byte[] pixels, int width, int height)
		{
			PixelFormat pixelFormat = PixelFormats.Bgr24;
			WriteableBitmap bitmap = new WriteableBitmap(width, height, dpi_X, dpi_Y, pixelFormat, null);
			bitmap.WritePixels(new Int32Rect(0, 0, width, height), pixels, getStride(bitmap), 0);
			return bitmap;
		}

		/// <summary>
		/// Возвращает битовую карту для изображения.
		/// </summary>
		/// <param name="image">Изображение в формате Image.</param>
		/// <returns>Битовая карта в формате WriteableBitmap.</returns>
		private static WriteableBitmap GetBitmap(Image image)
		{
			var source = image.Source as WriteableBitmap;

			if (source == null)
			{
				throw new Exception("image.Source не является WriteableBitmap");
			}

			return source;
		}

		/// <summary>
		/// Инвертирует значения в байтовом массиве.
		/// </summary>
		/// <param name="array">Байтовый массив.</param>
		public static void Invert(byte[] array)
		{
			int count = array.Length;
			for(int i = 0; i < count; i++)
			{
				array[i] = Convert.ToByte(255 - array[i]);
			}
		}

		/// <summary>
		/// Осуществляет декомпозицию изображения на каналы.
		/// </summary>
		/// <param name="image">Изображение в формате Image.</param>
		/// <returns>Кортеж каналов.</returns>
		public static (byte[] red, byte[] green, byte[] blue) decomposeImageToRGB(Image image)
		{
			return decomposeSourceToRGB(GetBitmap(image));
		}

		/// <summary>
		/// Осуществляет декомпозицию изображения на каналы.
		/// </summary>
		/// <param name="source">Битовая карта изображения.</param>
		/// <returns>Кортеж каналов.</returns>
		public static (byte[] red, byte[] green, byte[] blue) decomposeSourceToRGB(BitmapSource source)
		{
			byte[] byteArray = null;
			if (source.Format == PixelFormats.Bgr24)
			{
				byteArray = ImageHelper.BitmapSourceToArray(source);
			}
			else
			{ 
				FormatConvertedBitmap Bgr24BitmapSource = new FormatConvertedBitmap();

				// BitmapSource objects like FormatConvertedBitmap can only have their properties
				// changed within a BeginInit/EndInit block.
				Bgr24BitmapSource.BeginInit();

				// Use the BitmapSource object defined above as the source for this new 
				// BitmapSource (chain the BitmapSource objects together).
				Bgr24BitmapSource.Source = source;

				// Set the DestinationFormat
				Bgr24BitmapSource.DestinationFormat = PixelFormats.Bgr24;
				Bgr24BitmapSource.EndInit();

				byteArray = ImageHelper.BitmapSourceToArray(Bgr24BitmapSource);
			}

			int count = source.PixelWidth * source.PixelHeight;

            byte[] red = new byte[count];
            byte[] green = new byte[count];
            byte[] blue = new byte[count];

			int i = 0;
			int k = 0;

			int length = byteArray.Length;


			while (i < length)
			{
				blue[k] = byteArray[i++];
				green[k] = byteArray[i++];
				red[k] = byteArray[i++];
				k++;
			}
            return (red, green, blue);
		}

		/// <summary>
		/// Увеличивает значение массива на заданное значение.
		/// </summary>
		/// <param name="output">Массив смещенных значений.</param>
		/// <param name="input">Исходный массив.</param>
		/// <param name="offset">Смещение.</param>
		/// <exception cref="Exception">Выбрасывается при разной длине массивов.</exception>
		/// <exception cref="ArgumentOutOfRangeException">Выбрасывается при смещении большем 255 или меньшем -255.</exception>
		public static void shiftArrValues(byte[] output, byte[] input, int offset)
		{
			int length = input.Length;
			if(output.Length != length)
			{
				throw new Exception("Массивы должны быть одинаковой длины.");
			}

			if (offset > 255 || offset < -255)
			{
				throw new ArgumentOutOfRangeException();
			}
			
			int value;
			for (int i = 0; i < length; i++)
			{
				value = input[i] + offset;
				output[i] = ByteConverter.ToByte(value);
			}
		}

		/// <summary>
		/// Создает битовую карту на основе RGB каналов изображения.
		/// </summary>
		/// <param name="red">Красный канал.</param>
		/// <param name="green">Зеленый канал.</param>
		/// <param name="blue">Синий канал.</param>
		/// <param name="height">Высота изображения.</param>
		/// <param name="width">Ширина изображения.</param>
		/// <returns>Битовая карта.</returns>
		/// <exception cref="Exception">Выбрасывается при разной длине массивов.</exception>
		public static BitmapSource composeBitmapByRGB(byte[] red, byte[] green, byte[] blue, int height, int width)
		{
			PixelFormat pixelFormat = PixelFormats.Bgr24;

			if (blue.Length != red.Length
			 || blue.Length != green.Length)
			{
				throw new Exception("Blue, green, red, массивы должны быть одинаковой длины.");
			}
			
			int pixelsCount = blue.Length;
			int bytesCount = pixelsCount * 3;
			int i = 0;
			int k = 0;
			var byteArray = new byte[bytesCount];
			while (i < bytesCount)
			{
				byteArray[i++] = blue[k];
				byteArray[i++] = green[k];
				byteArray[i++] = red[k];
				k++;
			}

			return BitmapSourceFromArray(byteArray, width, height);
		}

		/// <summary>
		/// Создает битовую карту на основе YCbCr компнент изображения.
		/// </summary>
		/// <param name="yBytes">Компонента яркости.</param>
		/// <param name="cbBytes">Синяя цветоразностная компонента.</param>
		/// <param name="crBytes">Красная цветоразностная компонента.</param>
		/// <param name="height">Высота изображения.</param>
		/// <param name="width">Ширина изображения.</param>
		/// <returns></returns>
		public static BitmapSource composeBitmapFromYCbCr(byte[] yBytes, byte[] cbBytes, byte[] crBytes, int height, int width)
		{
			(byte[] r, byte[] g, byte[] b) = ColorSpaceConverter.convertYCbCrToRGB(yBytes, cbBytes, crBytes);
			return ImageHelper.composeBitmapByRGB(r, g, b, height, width);
		}

		/// <summary>
		/// Сжимает диапазон значений.
		/// </summary>
		/// <param name="input">Массив для сжатия.</param>
		/// <param name="min">Нижняя граница нового диапазона значений.</param>
		/// <param name="max">Верхняя граница нового диапазона значений.</param>
		/// <param name="result">Массив со сжатым диапазоном значений.</param>
		public static void ReduceRange(byte[] input, byte min, byte max, byte[] result)
		{
			int length = input.Length;
			if (result == null || result.Length != length)
			{
				result = new byte[length];
			}
			for(int i = 0; i < length; i++)
			{
				result[i] = Convert.ToByte(min + input[i] * (max - min) / 255);
			}
		}

		/// <summary>
		/// Изменяет контраст изображения.
		/// </summary>
		/// <param name="y">Исходная компонента яркости.</param>
		/// <param name="yOut">Измененная компонента яркости.</param>
		/// <param name="coefficient">Коэфициент изменения контраста.</param>
		public static void ChangeContrast(byte[] y, out byte[] yOut, double coefficient)
		{
			int length = y.Length;
			yOut = new byte[length];
			
			double averageBrightness = 0;
			foreach (byte b in y)
			{
				averageBrightness += b;
			};
			averageBrightness /= length;
			double value;
			for (int i = 0; i < length; i++)
			{
				value = averageBrightness + coefficient * (y[i] - averageBrightness);
				yOut[i] = ByteConverter.ToByte(value);
			}
		}

		/// <summary>
		/// Выделяет области со значением заданного канала Y, Cb или Cr изображения внутри диапазона значений.
		/// </summary>
		/// <param name="yOutput">Обработанная компонента яркости.</param>
		/// <param name="cbOutput">Обработанная синяя цветоразностная компонента</param>
		/// <param name="crOutput">Обработанная красная цветоразностная компонента.</param>
		/// <param name="y">Компонента яркости.</param>
		/// <param name="cb">Синяя цветоразностная компонента.</param>
		/// <param name="cr">Красная цветоразностная компонента.</param>
		/// <param name="left">Левая граница диапазона.</param>
		/// <param name="right">Правая граница диапазона.</param>
		/// <param name="channel">Тип канала, по которому выделяется избражения.</param>
		/// <exception cref="Exception">Выбрасывается при передаче типа компоненты отличной от Y, Cb, Cr.</exception>
		public static void SelectByYCbCr(byte[] yOutput, byte[] cbOutput, byte[] crOutput, byte[] y, byte[] cb, byte[] cr, byte left, byte right, ImageComponents channel)
		{
			byte[] comparableArray;
			switch(channel)
			{
				case ImageComponents.Y:
					comparableArray = y;
					break;
				case ImageComponents.Cb:
					comparableArray = cb;
					break;
				case ImageComponents.Cr:
					comparableArray = cr;
					break;
				default:
					throw new Exception("Ожидается передача следующих видов компонент: Y, Cb, Cr.");
			}

			int length = comparableArray.Length;
			byte value;
			for (int i = 0; i < length; i++)
			{
				value = comparableArray[i];
				if (left < value && value < right)
				{
					yOutput[i] = y[i];
					cbOutput[i] = cb[i];
					crOutput[i] = cr[i];
				}
				else
				{
					yOutput[i] = 0;
					cbOutput[i] = 128;
					crOutput[i] = 128;
				}
			}
		}

		/// <summary>
		/// Выделяет области со значением заданного канала изображения R, G или B внутри диапазона значений.
		/// </summary>
		/// <param name="rOutput">Обработанный красный канал.</param>
		/// <param name="gOutput">Обработанный зеленый канал.</param>
		/// <param name="bOutput">Обработанный синий канал.</param>
		/// <param name="r">Красный канал.</param>
		/// <param name="g">Зеленый канал.</param>
		/// <param name="b">Синий канал.</param>
		/// <param name="left">Левая граница диапазона.</param>
		/// <param name="right">Правая граница диапазона.</param>
		/// <param name="channel">Тип канала, по которому выделяется избражения.</param>
		/// <exception cref="Exception">Выбрасывается при передаче типа компоненты отличной от R, G, B.</exception>
		public static void SelectByRGB(byte[] rOutput, byte[] gOutput, byte[] bOutput, byte[] r, byte[] g, byte[] b, byte left, byte right, ImageComponents channel)
		{

			byte[] comparableArray;
			switch (channel)
			{
				case ImageComponents.Red:
					comparableArray = r;
					break;
				case ImageComponents.Green:
					comparableArray = g;
					break;
				case ImageComponents.Blue:
					comparableArray = b;
					break;
				default:
					throw new Exception("Ожидается передача следующих видов компонент: R, G, B.");
			}

			byte value;
			int length = comparableArray.Length;
			for (int i = 0; i < length; i++)
			{
				value = comparableArray[i];
				if (left < value && value < right)
				{
					rOutput[i] = r[i];
					gOutput[i] = g[i];
					bOutput[i] = b[i];
				}
				else
				{
					rOutput[i] = 0;
					gOutput[i] = 0;
					bOutput[i] = 0;
				}
			}
		}

		/// <summary>
		/// Изменяет размер изображения.
		/// </summary>
		/// <param name="bitmap">Битовая карта изображения.</param>
		/// <param name="newWidth">Новая ширина изображения.</param>
		/// <param name="newHeight">Новая высота изображения.</param>
		/// <returns></returns>
		public static WriteableBitmap ResizeBitmap(BitmapSource bitmap, double newWidth, double newHeight)
		{
			var resized = new TransformedBitmap(bitmap, new ScaleTransform(	
				newWidth / bitmap.PixelWidth,
				newHeight / bitmap.PixelHeight
				)
			);
			return new WriteableBitmap(resized);
		}

		/// <summary>
		/// Возвращает битовую карту изображения.
		/// </summary>
		/// <param name="image">Изображение в формате Image.</param>
		/// <returns>Битовая карта.</returns>
		/// <exception cref="Exception">Выбрасывается если image.Source не является WriteableBitmap.</exception>
		public static WriteableBitmap GetWritableBitmap(this Image image)
		{
			var bitmap = image.Source as WriteableBitmap;
			if (bitmap == null)
			{
				throw new Exception("image.Source не является WriteableBitmap");
			}
			return bitmap;
		}

		/// <summary>
		/// Осуществляет свертку изображения.
		/// </summary>
		/// <param name="inImage">Изображение (нужно для получение его ширины и высоты).</param>
		/// <param name="kernel">Ядро свертки.</param>
		/// <param name="R">Красный канал.</param>
		/// <param name="G">Зеленый канал.</param>
		/// <param name="B">Синий канал.</param>
		public static void Convol(Image inImage, double[,] kernel, byte[] R, byte[] G, byte[] B)
		{
			byte[] inputBytesR = R;
			byte[] inputBytesG = G;
			byte[] inputBytesB = B;

			R = new byte[inputBytesR.Length];
			G = new byte[inputBytesG.Length];
			B = new byte[inputBytesB.Length];

			int width = GetBitmap(inImage).PixelWidth;
			int height = GetBitmap(inImage).PixelHeight;


			int kernelWidth = kernel.GetLength(0);
			int kernelHeight = kernel.GetLength(1);

			for (int x = 0; x < width; x++)
			{
				for (int y = 0; y < height; y++)
				{
					double rSum = 0, gSum = 0, bSum = 0, kSum = 0;

					for (int i = 0; i < kernelWidth; i++)
					{
						for (int j = 0; j < kernelHeight; j++)
						{
							int pixelPosX = x + (i - (kernelWidth / 2));
							int pixelPosY = y + (j - (kernelHeight / 2));
							if ((pixelPosX < 0) ||
								(pixelPosX >= width) ||
								(pixelPosY < 0) ||
								(pixelPosY >= height)) continue;

							byte r = inputBytesR[width * pixelPosY + pixelPosX];
							byte g = inputBytesG[width * pixelPosY + pixelPosX];
							byte b = inputBytesB[width * pixelPosY + pixelPosX];

							double kernelVal = kernel[i, j];

							rSum += r * kernelVal;
							gSum += g * kernelVal;
							bSum += b * kernelVal;

							kSum += kernelVal;
						}
					}

					if (kSum <= 0) kSum = 1;

					rSum /= kSum;
					if (rSum < 0) rSum = 0;
					if (rSum > 255) rSum = 255;

					gSum /= kSum;
					if (gSum < 0) gSum = 0;
					if (gSum > 255) gSum = 255;

					bSum /= kSum;
					if (bSum < 0) bSum = 0;
					if (bSum > 255) bSum = 255;

					
					
					
					R[width * y + x] = (byte)rSum;
					G[width * y + x] = (byte)gSum;
					B[width * y + x] = (byte)bSum;
				}
			}
		}

		/// <summary>
		/// Преобразует формат битовой карты в Bgr24.
		/// </summary>
		/// <param name="bitmap">Битовая карта.</param>
		/// <returns>Изображение с измененным форматом.</returns>
        public static FormatConvertedBitmap ConvertToBgr24(BitmapSource bitmap)
        {
            FormatConvertedBitmap newFormatedBitmap = new FormatConvertedBitmap();
            newFormatedBitmap.BeginInit();
            newFormatedBitmap.Source = bitmap;
            newFormatedBitmap.DestinationFormat = PixelFormats.Bgr24;
            newFormatedBitmap.EndInit();
            return newFormatedBitmap;
        }
    }
}
