﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace WpfGraphicsEditor
{
	/// <summary>
	/// Преобразует значение Enum в значение видимости. Используется для упрощения привязки в XAML коде.
	/// </summary>
	class EnumToVisibilityConverter : IValueConverter
	{
		/// <summary>
		/// Если value равно parameter то возвращается то значение Visibility.Visible (видимо), иначе Visibility.Collapsed (свернуто).
		/// </summary>
		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			return ((Enum)value).Equals((Enum)parameter) ? Visibility.Visible : Visibility.Collapsed;
		}

		/// <summary>
		/// Ничего не делает.
		/// </summary>
		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			return Binding.DoNothing;
		}
	}
}
