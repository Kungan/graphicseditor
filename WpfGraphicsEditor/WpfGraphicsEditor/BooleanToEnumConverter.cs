﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows;

namespace WpfGraphicsEditor
{
	/// <summary>
	/// Конвертер типов bool и enum. Используется для упрощения привязки в XAML коде.
	/// </summary>
	class BooleanEnumConverter : IValueConverter
	{
		/// <summary>
		/// Сравнивает перечисление value с перечислением parameter.
		/// </summary>
		/// <returns></returns>
		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			return ((Enum)value).Equals((Enum)parameter);
		}

		/// <summary>
		/// Если <see langword="true"/>, возвращает parameter.
		/// </summary>
		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			return value.Equals(true) ? parameter : Binding.DoNothing;
		}
	}

}
