﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfGraphicsEditor
{
	/// <summary>
	/// Тип кисти.
	/// </summary>
	public enum BrushType
	{
		/// <summary>
		/// Сплошная кисть.
		/// </summary>
		Solid,

		/// <summary>
		/// Линейный градиент.
		/// </summary>
		LinearGradient,

		/// <summary>
		/// Радиальный градиент.
		/// </summary>
		RadialGradient
	}
}
