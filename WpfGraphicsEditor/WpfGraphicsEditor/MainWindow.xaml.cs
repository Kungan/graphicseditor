﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfGraphicsEditor.Model;
using WpfGraphicsEditor.View;
using WpfGraphicsEditor.ViewModel;
using Xceed.Wpf.AvalonDock.Layout;
using Xceed.Wpf.Toolkit.PropertyGrid;

namespace WpfGraphicsEditor
{
	/// <summary>
	/// Логика взаимодействия для MainWindow.xaml
	/// </summary>
	[NotifyPropertyChanged]
	public partial class MainWindow : Window
	{
		/// <summary>
		/// Конструктор.
		/// </summary>
		public MainWindow()
		{
			ContourWidth = 2;
			InitializeComponent();

            //Можно раскомментировать для тестирования рисования чтобы не создавать новый рисунок при каждом запуске.
            //CreateNewDocument(1200, 700);

            selectionRectangle = new Rectangle();
            selectionRectangle.Stroke = new SolidColorBrush(Color.FromRgb(138, 190, 226));
            selectionRectangle.StrokeThickness = 1.4;
            selectionRectangle.StrokeDashArray = new DoubleCollection() { 4, 2 };

            BrushContourVM = new BrushControlVM(Colors.Black);
            BrushFillVM = new BrushControlVM(Colors.Transparent);
        }

		/// <summary>
		/// Активный документ.
		/// </summary>
		private LayoutDocument activeDocument;

		/// <summary>
		/// Активный документ.
		/// </summary>
        public LayoutDocument ActiveDocument
		{
			get { return activeDocument; }

			set
			{
				RemoveClipRectangle();
				activeDocument = value;
				this.DataContext = (activeDocument == null) ? null : CurrentPane;
			}
		}

		/// <summary>
		/// Текущий холст изображения.
		/// </summary>
        private Pane CurrentPane => activeDocument.Content as Pane;

		/// <summary>
		/// Режим рисования.
		/// </summary>
		public ToolMode Mode
		{
			get { return toolMode; }
			set
			{
				toolMode = value;
				selectShape(null);
				var pane = (this.DataContext as Pane);
				if(pane != null)
				{
					InkCanvas canvas = pane.canvas;
					switch (value)
					{
						case ToolMode.Ink:
							canvas.EditingMode = InkCanvasEditingMode.Ink;
							break;
						default:
							canvas.EditingMode = InkCanvasEditingMode.None;
							break;
					}

					InkCanvas imageCanvas = (this.DataContext as Pane).imageCanvas;
					switch (value)
					{
						case ToolMode.Eraser:
							imageCanvas.EditingMode = InkCanvasEditingMode.Ink;
							break;
						default:
							imageCanvas.EditingMode = InkCanvasEditingMode.None;
							break;
					}
				}
			}
		}

		/// <summary>
		/// Ширина контура выделения.
		/// </summary>
		public double? ContourWidth
		{
			get { return contourWidth; }
			set
			{
				contourWidth = value;
				if(selectedShape != null)
				{
					selectedShape.StrokeThickness = value.Value;
				}
			}
		}
		
		/// <summary>
		/// Модель представления кисти заливки.
		/// </summary>
		public BrushControlVM BrushFillVM { get; set; }

		/// <summary>
		/// Модель представления кисти контура.
		/// </summary>
		public BrushControlVM BrushContourVM { get; set; }

		/// <summary>
		/// Открытие изображения.
		/// </summary>
		private void OpenCommand_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			Stream myStream = null;
			OpenFileDialog openFileDialog = new OpenFileDialog();
			openFileDialog.RestoreDirectory = true;
			openFileDialog.Filter = "image files |*.bmp;*.gif;*.jpg;*.jpeg;*.png;*.tiff|Все файлы|*.*";
			if (openFileDialog.ShowDialog() == true)
			{
				BitmapImage src = new BitmapImage();

				try
				{
					if ((myStream = openFileDialog.OpenFile()) != null)
					{
						src.BeginInit();
						src.StreamSource = myStream;
						src.EndInit();
					}
				}
				catch (Exception ex)
				{
					MessageBox.Show("Ошибка. Не могу открыть файл. Текст ошибки: " + ex.Message);
				}
				WriteableBitmap source = new WriteableBitmap(src);

				var pane = new Pane(source);
				AddDocument(openFileDialog.SafeFileName, pane);
			}
		}

		/// <summary>
		/// Создание изображения.
		/// </summary>
		private void NewCommand_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			var window = new WidthHeightWindow("Создать рисунок", 900, 500);
			if (window.ShowDialog() == true)
			{
				int? width = window.RequiredWidth;
				int? height = window.RequiredHeight;
				CreateNewDocument(width.Value, height.Value);
			};
		}

		/// <summary>
		/// Проверяет, доступны ли операции с изображением.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void checkImageAviability(object sender, DependencyPropertyChangedEventArgs e)
		{
            bool isImageAviable = e.NewValue is Pane pane && pane.Image != null;
            (sender as FrameworkElement).IsEnabled = isImageAviable;
		}

		/// <summary>
		/// Создает новый документ.
		/// </summary>
		private void CreateNewDocument(int width, int height)
		{
			string title = "Новый рисунок " + width.ToString() + "x" + height.ToString();
			var pane = new Pane(width, height);
			AddDocument(title, pane);
		}
		
		/// <summary>
		/// Добавляет новый документ.
		/// </summary>
		/// <param name="title">Заголовок.</param>
		/// <param name="pane">Холст.</param>
		/// <returns></returns>
		private LayoutDocument AddDocument(string title, Pane pane)
		{
            var document = new LayoutDocument
            {
                Content = pane
            };
            document.IsSelectedChanged += Document_IsSelectedChanged;
			documentPane.Children.Add(document);
			//Нужно обновлять последний документ именно после того, как на на него добавлено полотно.
			//Иначе не синхронизируются опции цветовых компонентов.
			documentPane.SelectedContentIndex = documentPane.IndexOf(document);
			document.Title = title;
			document.Closed += Document_Closed;

			pane.canvas.MouseDown += canvas_MouseDown;
			pane.canvas.MouseMove += canvas_MouseMove;
			pane.canvas.MouseUp += canvas_MouseUp;

			pane.canvas.AddHandler(InkCanvas.PreviewMouseDownEvent, new MouseButtonEventHandler(Canvas_PreviewMouseDown), true);
			pane.imageCanvas.AddHandler(InkCanvas.PreviewMouseDownEvent, new MouseButtonEventHandler(ImageCanvas_PreviewMouseDown), true);

			return document;
		}

		/// <summary>
		/// Обработчик, выполняющийся перед нажатием мыши на холст с ластиком.
		/// </summary>
		private void Canvas_PreviewMouseDown(object sender, MouseButtonEventArgs e)
		{
			InkCanvas canvas = sender as InkCanvas;
			canvas.DefaultDrawingAttributes.Color = BrushContourVM.SelectedColor;
			canvas.DefaultDrawingAttributes.Height = contourWidth.Value;
			canvas.DefaultDrawingAttributes.Width = contourWidth.Value;
		}

		/// <summary>
		/// Обработчик, выполняющийся перед нажатием мыши на холст с изображением.
		/// </summary>
		private void ImageCanvas_PreviewMouseDown(object sender, MouseButtonEventArgs e)
		{
			InkCanvas canvas = sender as InkCanvas;
			canvas.DefaultDrawingAttributes.Height = contourWidth.Value;
			canvas.DefaultDrawingAttributes.Width = contourWidth.Value;
		}

		/// <summary>
		/// Обработчик закрытия документа.
		/// </summary>
		private void Document_Closed(object sender, EventArgs e)
		{
			if (ActiveDocument == sender as LayoutDocument)
				ActiveDocument = null;
		}

		/// <summary>
		/// Обработчик изменения выбранного документа. 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Document_IsSelectedChanged(object sender, EventArgs e)
		{
			ActiveDocument = sender as LayoutDocument;
		}

		/// <summary>
		/// Сохранение изображения.
		/// </summary>
		private void SaveCommand_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			SaveFileDialog saveFileDialog = new SaveFileDialog();
			saveFileDialog.Filter = "BMP|*.bmp|JPEG|*.jpeg|TIFF|*.tiff|GIF|*.gif|PNG|*.png";
			saveFileDialog.RestoreDirectory = true;

			if (saveFileDialog.ShowDialog() == true)
			{
				Pane pane = ActiveDocument.Content as Pane;
				RenderTargetBitmap renderTargetBitmap = ImageHelper.ToImageSource(pane.canvas);
				FileStream fileStream = new FileStream(saveFileDialog.FileName, FileMode.Create);
				
				BitmapEncoder encoder = null;
				switch (saveFileDialog.FilterIndex)
				{
					case 1:
						encoder = new BmpBitmapEncoder();
						break;
					case 2:
						encoder = new JpegBitmapEncoder();
						break;
					case 3:
						encoder = new TiffBitmapEncoder();
						break;
					case 4:
						encoder = new GifBitmapEncoder();
						break;
					case 5:
						encoder = new PngBitmapEncoder();
						break;
				}

				encoder.Frames.Add(BitmapFrame.Create(renderTargetBitmap));
				encoder.Save(fileStream);
				fileStream.Close();
			}
		}

		/// <summary>
		/// Определяет, могут ли быть выполены операции на холсте.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void PaneOperations_CanExecute(object sender, CanExecuteRoutedEventArgs e)
		{
			e.CanExecute = ActiveDocument != null;
		}

		/// <summary>
		/// Определяет, можно ли выполнить обрезку изображения.
		/// </summary>
		private void Clip_CanExecute(object sender, CanExecuteRoutedEventArgs e)
		{
			e.CanExecute = ActiveDocument != null && (ActiveDocument.Content as Pane).Image != null && 
				clipRectangle != null && clipRectangle.Width > 0 && clipRectangle.Height > 0;
		}

		/// <summary>
		/// Определяет, можно ли инвертировать цвета.
		/// </summary>
		private void InvertColors_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			CurrentPane.InvertColors();
		}

		/// <summary>
		/// Определяет, можно ли выполнить операции над изображением.
		/// </summary>
		private void ImageOperations_CanExecute(object sender, CanExecuteRoutedEventArgs e)
		{
			e.CanExecute = ActiveDocument != null && (ActiveDocument.Content as Pane).Image != null;
		}

		
		/// <summary>
		/// Определяет, можно ли применить изменения.
		/// </summary>
		private void Apply_CanExecute(object sender, CanExecuteRoutedEventArgs e)
		{
			e.CanExecute = ActiveDocument != null && (ActiveDocument.Content as Pane).IsChanged;
		}
		
		/// <summary>
		/// Текущая фигура
		/// </summary>
		private Shape currentShape;

		/// <summary>
		/// Режим рисования.
		/// </summary>
		private ToolMode toolMode;

		/// <summary>
		/// Начальная точка рисования.
		/// </summary>
		Point start;

		/// <summary>
		/// Прямоугольник обрезки изображения.
		/// </summary>
		private Rectangle clipRectangle;

		/// <summary>
		/// Определяет, изменяется ли размер прямоугольника обрезки.
		/// </summary>
		bool isClipRectangleResizing;

		/// <summary>
		/// Обработчик нажатия на холст.
		/// </summary>
		private void canvas_MouseDown(object sender, MouseButtonEventArgs e)
		{
			//В режиме резинки при нажатии на фигуру в canvas не происходит стирания во вложенном imageCanvas, т. к. фигура перехватывает нажатие.
			//Чтобы не произошло захвата мыши в родительском canvas, проверяем режим.
			if(Mode == ToolMode.Eraser)
			{
				return;
			}

			var canvas = sender as InkCanvas;
			start = e.GetPosition(sender as InkCanvas);
			Mouse.Capture(canvas);
			RemoveClipRectangle();
			selectShape(null);

			switch (Mode)
			{
				case ToolMode.Line:
					var line = new Line();
					currentShape = line;
					line.X1 = line.X2 = start.X;
					line.Y1 = line.Y2 = start.Y;
					break;
				case ToolMode.Rectangle:
					currentShape = new Rectangle();
					SetPositionOnStart(currentShape);
					break;
				case ToolMode.Ellipse:
					currentShape = new Ellipse();
					SetPositionOnStart(currentShape);
					break;
				case ToolMode.Polyline:
					if (!(currentShape is Polyline))
					{
						currentShape = new Polyline();
						(currentShape as Polyline).Points.Add(start);
					}
					(currentShape as Polyline).Points.Add(start);
					if (e.ClickCount > 1)
					{
						finishDrawing();
					}
					break;
				case ToolMode.Polygon:
					if (!(currentShape is Polygon))
					{
						currentShape = new Polygon();
						(currentShape as Polygon).Points.Add(start);
					}
					(currentShape as Polygon).Points.Add(start);
					if (e.ClickCount > 1)
					{
						finishDrawing();
					}
					break;
				case ToolMode.Clip:
					clipRectangle = new Rectangle();
					clipRectangle.StrokeThickness = 2;
					clipRectangle.StrokeDashArray = new DoubleCollection() { 3, 1 };
					clipRectangle.Stroke = new SolidColorBrush(Color.FromRgb(138, 190, 226));
					SetPositionOnStart(clipRectangle);
					canvas.Children.Add(clipRectangle);
					isClipRectangleResizing = true;
					break;
				case ToolMode.Select:
					Mouse.Capture(null);
					break;
			}

			if (currentShape != null && currentShape.Parent == null && Mode != ToolMode.Clip)
			{
				currentShape.Stroke = BrushContourVM.CurrentBrush.Clone();
				currentShape.Fill = currentShape is Polyline ? Brushes.Transparent : BrushFillVM.CurrentBrush.Clone();
				currentShape.StrokeThickness = ContourWidth.Value;
				currentShape.MouseDown += Shape_MouseDown;
				canvas.Children.Add(currentShape);
			}
			e.Handled = true;
		}

		/// <summary>
		/// Устанавливает начальные параметры фигуры.
		/// </summary>
		/// <param name="shape">Фигура.</param>
		private void SetPositionOnStart(Shape shape)
		{
			shape.Margin = new Thickness(start.X, start.Y, 0, 0);
			shape.Width = 0;
			shape.Height = 0;
		}

		/// <summary>
		/// Заканчивает рисование.
		/// </summary>
		private void finishDrawing()
		{
			selectShape(currentShape);
			currentShape = null;
			Mouse.Capture(null);
		}

		/// <summary>
		/// Точка окончания рисования.
		/// </summary>
		Point end;

		/// <summary>
		/// Обработчик перемещения мыши по холсту.
		/// </summary>
		private void canvas_MouseMove(object sender, MouseEventArgs e)
		{
			end = e.GetPosition(sender as InkCanvas);
			if (currentShape != null)
			{
				switch (Mode)
				{
					case ToolMode.Line:
						var line = currentShape as Line;
						line.X2 = end.X;
						line.Y2 = end.Y;
						break;
					case ToolMode.Ellipse:
					case ToolMode.Rectangle:
						SetPositionByStartEnd(currentShape);
						break;
					case ToolMode.Polyline:
						Polyline poliline = currentShape as Polyline;
						poliline.Points[poliline.Points.Count - 1] = end;
						break;
					case ToolMode.Polygon:
						Polygon polygon = currentShape as Polygon;
						polygon.Points[polygon.Points.Count - 1] = end;
						break;
				}
			}

			if(clipRectangle != null && Mode == ToolMode.Clip && isClipRectangleResizing)
			{
				SetPositionByStartEnd(clipRectangle);
			}
		}

		/// <summary>
		/// Устанавливает позицию фигуры.
		/// </summary>
		/// <param name="shape">Фигура.</param>
		private void SetPositionByStartEnd(Shape shape)
		{
			double width = end.X - start.X;
			double height = end.Y - start.Y;
			double left = width > 0 ? start.X : end.X;
			double top = height > 0 ? start.Y : end.Y;
			shape.Margin = new Thickness(left, top, 0, 0);
			shape.Width = Math.Abs(width);
			shape.Height = Math.Abs(height);
		}

		/// <summary>
		/// Обработчик прекращения нажатия на холст мышью.
		/// </summary>
		private void canvas_MouseUp(object sender, MouseButtonEventArgs e)
		{
			if (currentShape != null && Mode != ToolMode.Polyline && Mode != ToolMode.Polygon)
			{
				//Рисование полигона и полилинии не заканчивается на MouseUp.
				finishDrawing();
			}
			if (Mode == ToolMode.Clip && clipRectangle != null)
			{
				isClipRectangleResizing = false;
				finishClipping();
			}
		}

		/// <summary>
		/// Заканчивает обрезку.
		/// </summary>
		private void finishClipping()
		{
			Mouse.Capture(null);
		}

		/// <summary>
		/// Удаляет прямоугольник обрезки.
		/// </summary>
		private void RemoveClipRectangle()
		{
			if (activeDocument != null && clipRectangle != null)
			{
				CurrentPane.canvas.Children.Remove(clipRectangle);
				clipRectangle = null;
			}
		}

		/// <summary>
		/// Выполняет обрезку изображения.
		/// </summary>
		private void Clip_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			var bitmap = CurrentPane.Image.Source as WriteableBitmap;
			int left = Convert.ToInt32(clipRectangle.Margin.Left);
			int x = left > 0 ? left : 0;
			int top = Convert.ToInt32(clipRectangle.Margin.Top);
			int y = top > 0 ? top : 0;
			int width = Convert.ToInt32(clipRectangle.Width);
			int height = Convert.ToInt32(clipRectangle.Height);

			if (left < 0)
			{
				width += left;
			}

			if (top < 0)
			{
				height += top;
			}

			if(x + width > bitmap.PixelWidth)
			{
				width = bitmap.PixelWidth - x;
			}

			if (y + height > bitmap.PixelHeight)
			{
				height = bitmap.PixelHeight - y;
			}

			var rectangle = new Int32Rect(x, y, width, height);
			var clippedBitmap = new WriteableBitmap(new CroppedBitmap(bitmap, rectangle));
			var pane = new Pane(clippedBitmap);
			string title = activeDocument.Title + " " + rectangle.Width + "x" + rectangle.Height;

			RemoveClipRectangle();
			AddDocument(title, pane);
		}

		/// <summary>
		/// Прямоугольник выделения.
		/// </summary>
		Rectangle selectionRectangle;

		/// <summary>
		/// Ширина контура.
		/// </summary>
		private double? contourWidth;

		/// <summary>
		/// Выбранная фигура.
		/// </summary>
		Shape selectedShape;

		/// <summary>
		/// Обработчик нажатия по фигуре.
		/// </summary>
		private void Shape_MouseDown(object sender, MouseButtonEventArgs e)
		{
			if (Mode != ToolMode.Select)
			{
				return;
			}
			e.Handled = true;
			selectShape(sender as Shape);
		}

		/// <summary>
		/// Выбирает  фигуру.
		/// </summary>
		/// <param name="shape">Фигура.</param>
		private void selectShape(Shape shape)
		{
			(selectionRectangle.Parent as InkCanvas)?.Children.Remove(selectionRectangle);

			selectedShape = shape;
			if (selectedShape != null)
			{
				selectedShape.SetBounds(selectionRectangle);
				(selectedShape.Parent as InkCanvas).Children.Add(selectionRectangle);
			}
		}

		/// <summary>
		/// Выполняет применения изменений.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Apply_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			CurrentPane.Apply();
		}

		/// <summary>
		/// Отменяет последние изменения.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Cancel_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			CurrentPane.Cancel();
		}

		/// <summary>
		/// Показывает окно градиентов.
		/// </summary>
		private void ShowGradients_Executed(object sender, ExecutedRoutedEventArgs e)
		{
            var window = new GradientPreview(CurrentPane)
            {
            };
			window.Show();
		}

		/// <summary>
		/// Повышает четкость изображения.
		/// </summary>
		private void Clarity_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			CurrentPane.Clarity();
		}

		/// <summary>
		/// Выделяет границы.
		/// </summary>
		private void Borders_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			CurrentPane.Borders();
		}
		
		/// <summary>
		/// Размывает изображение.
		/// </summary>
		private void Blur_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			CurrentPane.Blur();
		}

		/// <summary>
		/// Снижает резкость.
		/// </summary>
        private void UnSharpening_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            CurrentPane.UnSharpening();
        }

		/// <summary>
		/// Гауссово размытие.
		/// </summary>
        private void GaussianBluring_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            CurrentPane.GaussianBluring();
        }

		/// <summary>
		/// Показывает линейку.
		/// </summary>
		private void Ruler_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			CurrentPane.ShowRuler();
		}

		/// <summary>
		/// Показывает сетку.
		/// </summary>
		private void Grid_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			CurrentPane.ShowGrid();
		}

		/// <summary>
		/// Изменяет размер изображения.
		/// </summary>
		private void Resize_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			WriteableBitmap input = CurrentPane.Image.GetWritableBitmap();
			var window = new WidthHeightWindow("Изменить размер изображения", input.PixelWidth, input.PixelHeight);
			if (window.ShowDialog() == true)
			{
				int width = window.RequiredWidth.Value;
				int height = window.RequiredHeight.Value;
				WriteableBitmap output = ImageHelper.ResizeBitmap(input, width, height);
				string title = activeDocument.Title + " " + output.PixelWidth.ToString() + "x" + output.PixelHeight.ToString();
				AddDocument(title, new Pane(output));
			}
		}

		/// <summary>
		/// Осуществляет растеризацию изображения.
		/// </summary>
		private void Rasterisation_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			selectShape(null);
			Pane pane = ActiveDocument.Content as Pane;
			RenderTargetBitmap renderTargetBitmap = ImageHelper.ToImageSource(pane.canvas);
			WriteableBitmap bitmap = new WriteableBitmap(renderTargetBitmap);
			string title = activeDocument.Title + " (растеризовано)";
			AddDocument(title, new Pane(bitmap));
		}

		/// <summary>
		/// Показывает окно сигнатуры изображения.
		/// </summary>
		private void Signature_Executed(object sender, ExecutedRoutedEventArgs e)
        {

            var window = new SignatureWindow()
            {
                DataContext = new SignatureGroupVM(CurrentPane.Image.Source as WriteableBitmap)
            };
            window.Show();
        }

		/// <summary>
		/// Открывает окно поиска контуров. 
		/// </summary>
        private void ContourSearch_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Pane pane = ActiveDocument.Content as Pane;
            WriteableBitmap origialBitmap = pane.Image.GetWritableBitmap();
            var contourSearchModel = new Model.ContoursSearch(origialBitmap);

            var window = new View.ContoursSearch
            {
                DataContext = new ContoursSearchVM(contourSearchModel)
            };

            window.Show();
        }

		/// <summary>
		/// Выравнивает изображение.
		/// </summary>
		private void AutoRotate_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Pane pane = ActiveDocument.Content as Pane;
            WriteableBitmap original = pane.Image.GetWritableBitmap();
            RenderTargetBitmap renderTargetBitmap = ImageHelper.ToImageSource(pane.canvas);
            string title = activeDocument.Title + " (автоповорот)";
            BitmapSource rotated = ImageRotator.AutoRotate(original);
            AddDocument(title, new Pane(rotated));
        }

		/// <summary>
		/// Модель представления для параметров конвертации цветовых пространств.
		/// </summary>
        private ColorSpaceParametersVM colorSpaceParametersVM = new ColorSpaceParametersVM();

		/// <summary>
		/// Окно настройки параметров конвертации цветовых пространств.
		/// </summary>
		private ColorSpaceParametersWindow colorSpaceParametersWindow;

		/// <summary>
		/// Показывает окно настройки параметров конвертации цветовых пространств.
		/// </summary>
		private void ColorSpaceParameters_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.colorSpaceParametersWindow = new ColorSpaceParametersWindow();
            colorSpaceParametersWindow.DataContext = this.colorSpaceParametersVM;
            colorSpaceParametersVM.PropertyChanged += ColorSpaceParametersVM_PropertyChanged;
            colorSpaceParametersWindow.ShowDialog();
            colorSpaceParametersVM.PropertyChanged -= ColorSpaceParametersVM_PropertyChanged;
        }

		/// <summary>
		/// Обработчик событий в окне настройки параметров конвертации цветовых пространств.
		/// </summary>
		private void ColorSpaceParametersVM_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "Apply":
                    foreach(LayoutDocument document in documentPane.Children)
                    {
                        if(document.Content is Pane pane)
                        {
                            pane.UpdateYCbCrByCurrentRGBChannels();
                        } 
                    }
                    break;
                case "Quit":
                    colorSpaceParametersWindow.Close();
                    break;
            }
        }
    }
}
