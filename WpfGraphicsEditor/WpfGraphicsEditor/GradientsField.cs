﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace WpfGraphicsEditor
{
	/// <summary>
	/// Класс представляющий поле градиентов.
	/// </summary>
	[NotifyPropertyChanged]
	public class GradientsField
	{
		/// <summary>
		/// Конструктор.
		/// </summary>
		/// <param name="pixelWidth">Ширина поля (в пикселях).</param>
		/// <param name="r">Канал красного цвета.</param>
		/// <param name="g">Канал зеленого цвета.</param>
		/// <param name="b">Канал синего цвета.</param>
		public GradientsField(int pixelWidth, byte[] r, byte[] g, byte[] b)
		{
			Field = ImageHelper.CalculateGradients(pixelWidth, r, g, b);
			Gradients = new ObservableCollection<KeyValuePair<string, float>>();
			CurrentRowColumn = new Tuple<int, int>(0, 0);
		}

		/// <summary>
		/// Текущие координаты выбранного градиента.
		/// </summary>
		private Tuple<int, int> currentRowColumn;

		/// <summary>
		/// Текущие координаты выбранного градиента.
		/// </summary>
		public Tuple<int, int> CurrentRowColumn
		{
			get { return currentRowColumn; }
			set
			{
				CurrentPixel = Field[value.Item1, value.Item2];
				currentRowColumn = value;
			}
		}

		/// <summary>
		/// Текущий пиксель.
		/// </summary>
		private PixelGradient currentPixel;

		/// <summary>
		/// Текущий пиксель.
		/// </summary>
		public PixelGradient CurrentPixel
		{
			get { return currentPixel; }
			set
			{
				currentPixel = value;
				Gradients.Clear();
				for(int i = 0; i < value.Gradients.Length; i++)
				{
					this.Gradients.Add(new KeyValuePair<string, float>(
						(i+1).ToString(), value.Gradients[i]));
				}
			}
		}

		/// <summary>
		/// Градиенты, соответвующие выбранному пикселю.
		/// </summary>
		public ObservableCollection<KeyValuePair<string, float>> Gradients
		{
			get; set;
		}

		/// <summary>
		/// Двумерный массив градиентов.
		/// </summary>
		public PixelGradient[,] Field { get; private set; }
	}
}
