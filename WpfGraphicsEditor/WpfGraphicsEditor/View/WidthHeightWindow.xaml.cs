﻿using System.Windows;

namespace WpfGraphicsEditor
{
	/// <summary>
	/// Логика взаимодействия для WidthHeightWindow.xaml
	/// </summary>
	public partial class WidthHeightWindow : Window
	{
		public WidthHeightWindow(string title, int defaultWidth, int defaultHeight)
		{
			this.RequiredWidth= defaultWidth;
			this.RequiredHeight = defaultHeight;
			this.Title = title;
			InitializeComponent();
		}

		public int? RequiredWidth
		{
			get; set;
		}
		public int? RequiredHeight
		{
			get; set;
		}

		private void buttonOK_Click(object sender, RoutedEventArgs e)
		{
			if(RequiredWidth == null || RequiredHeight == null)
			{
				MessageBox.Show("Введите ширину и высоту.", "Неверные данные", MessageBoxButton.OK, MessageBoxImage.Warning);
			}
			else
			{
				DialogResult = true;
			}
		}
	}
}
