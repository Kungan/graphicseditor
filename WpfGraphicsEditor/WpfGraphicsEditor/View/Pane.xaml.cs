﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Convolution.DSP;
using OpenCvSharp;
using OpenCvSharp.Util;
using OpenCvSharp.Extensions;
using System.IO;
using WpfGraphicsEditor.ViewModel;
using System.Collections.ObjectModel;
using WpfGraphicsEditor.Model;
//using OpenCvSharp

namespace WpfGraphicsEditor
{
	/// <summary>
	/// Логика взаимодействия для Canvas.xaml
	/// </summary>
	/// 
	partial class Pane : UserControl
	{
		/// <summary>
		/// Конструктор холста.
		/// </summary>
		/// <param name="width">Ширина.</param>
		/// <param name="height">Высота.</param>
		public Pane(int width, int height)
		{
			InitializeComponent();
			setPaneSize(width, height);
		}

		/// <summary>
		/// Конструктор.
		/// </summary>
		/// <param name="bitmap">Битовая карта изображения.</param>
		public Pane(BitmapSource bitmap)
		{
			InitializeComponent();
            Image image = new Image
            {
                Stretch = Stretch.Fill,
                Source = bitmap,
                Width = bitmap.PixelWidth,
                Height = bitmap.PixelHeight
            };
            InitializePane(image);
		}

		#region Изображение и массивы каналов
		/// <summary>
		/// Стандартное значение для синей и красной цветоразностных компонент.
		/// </summary>
		private const int emptyCbCrValue = 128;


		/// <summary>
		/// Изображение.
		/// </summary>
		public Image Image { get; private set; }

		/// <summary>
		/// Красный канал изображения.
		/// </summary>
		public byte[] Red { get; private set; }

		/// <summary>
		/// Зеленый канал изображения.
		/// </summary>
		public byte[] Green { get; private set; }

		/// <summary>
		/// Синий канал изображения.
		/// </summary>
		public byte[] Blue { get; private set; }

		/// <summary>
		/// Компонента яркости.
		/// </summary>
		public byte[] Y { get; private set; }

		/// <summary>
		/// Синяя цветоразностная компонента.
		/// </summary>
		public byte[] Cb { get; private set; }

		/// <summary>
		/// Красная цветоразностная компонента.
		/// </summary>
		public byte[] Cr { get; private set; }

		/// <summary>
		/// Красный канал изображения для предварительного просмотра.
		/// </summary>
		public byte[] RPreview { get; private set; }

		/// <summary>
		/// Зеленый канал изображения для предварительного просмотра.
		/// </summary>
		public byte[] GPreview { get; private set; }

		/// <summary>
		/// Синий канал изображения для предварительного просмотра.
		/// </summary>
		public byte[] BPreview { get; private set; }

		/// <summary>
		/// Измененная яркость для предварительного просмотра.
		/// </summary>
		private byte[] yPreview;

		/// <summary>
		/// Синяя цветоразностная компонента для предварительного просмотра.
		/// </summary>
		public byte[] CbPreview { get; private set; }

		/// <summary>
		/// Красная цветоразностная компонента для предварительного просмотра.
		/// </summary>
		public byte[] CrPreview { get; private set; }
		public bool IsChanged
		{
			get
			{
				if (Image == null) {
					return false;
				}
				return !Red.SequenceEqual(RPreview) || !Green.SequenceEqual(GPreview) || !Blue.SequenceEqual(BPreview);
			}
		}

		#endregion

		/// <summary>
		/// Применить изменения.
		/// </summary>
		public void Apply()
		{
			RPreview.CopyTo(Red, 0);
			GPreview.CopyTo(Green, 0);
			BPreview.CopyTo(Blue, 0);

			yPreview.CopyTo(Y, 0);
			CbPreview.CopyTo(Cb, 0);
			CrPreview.CopyTo(Cr, 0);
		}

		/// <summary>
		/// Изменяет размер холста.
		/// </summary>
		/// <param name="width">Ширина.</param>
		/// <param name="height">Высота.</param>
		private void setPaneSize(double width, double height)
		{
			imageCanvas.Width = width;
			imageCanvas.Height = height;
			canvas.Width = width;
			canvas.Height = height;
		}

		/// <summary>
		/// Инициализирует холст.
		/// </summary>
		/// <param name="image">Изображение, которое должно быть на холсте.</param>
		private void InitializePane(Image image)
        {
            #region Добавление изображения
            imageCanvas.Children.Add(image);
            setPaneSize(image.Width, image.Height);
            this.Image = image;
            #endregion

            #region Инициализация опций для отображения каналов
            isRGB = true;

            isRedActive = true;
            isGreenActive = true;
            isBlueActive = true;

            isYActive = true;
            isCrActive = true;
            isCbActive = true;
            #endregion

            #region Заполнение массивов с каналами изображения
            (Red, Green, Blue) = ImageHelper.decomposeImageToRGB(image);

            RPreview = new byte[Red.Length];
            Red.CopyTo(RPreview, 0);
            GPreview = new byte[Green.Length];
            Green.CopyTo(GPreview, 0);
            BPreview = new byte[Blue.Length];
            Blue.CopyTo(BPreview, 0);


            UpdateYCbCrByCurrentRGBChannels();
            #endregion

            #region Инициализация опций для изменения яркости
            brightnessOffset = 0;
			#endregion

			#region Инициализация опций для изменения насыщенности
			ColorfulnessVM.PropertyChanged += ColorfulnessVM_PropertyChanged;
			#endregion

			#region Инициализация опций для изменения контрастности
			contrastCoefficient = 1;
            #endregion

            #region Инициализация опций для сжатия цветового диапазона
            isRedReducing = true;
            isGreenReducing = true;
            isBlueReducing = true;

            minReducingValue = 0;
            maxReducingValue = 255;
            #endregion

            #region Инициализация параметров для сетки и линейки
            rulerHeight = -1;
            rulerWidth = -1;
            gridHeight = -1;
            gridWidth = -1;
            #endregion

            #region Инициализация масштаба
            ScaleViewModel.PropertyChanged += ScaleViewModel_PropertyChanged;
            #endregion

        }

		/// <summary>
		/// Обновляет YCbCr компоненты на основе текущих каналов RGB.
		/// </summary>
		public void UpdateYCbCrByCurrentRGBChannels()
        {
            (Y, Cb, Cr) = ColorSpaceConverter.convertRGBToYCbCr(Red, Green, Blue);
            yPreview = new byte[Y.Length];
            Y.CopyTo(yPreview, 0);
            CbPreview = new byte[Cb.Length];
            Cb.CopyTo(CbPreview, 0);
            CrPreview = new byte[Cr.Length];
            Cr.CopyTo(CrPreview, 0);
            previewImage();
        }

		/// <summary>
		/// Осуществляет предпросмотр изображения по RGB каналам.
		/// </summary>
        private void previewImageByRGB()
		{
			updateImageByRGB(RPreview, GPreview, BPreview);
			(yPreview,  CbPreview, CrPreview) = ColorSpaceConverter.convertRGBToYCbCr(RPreview, GPreview, BPreview);
		}

		/// <summary>
		/// Осуществляет предпросмотр изображения по YCbCr каналам.
		/// </summary>

		private void previewImageByYCbCr()
		{
			updateImageByYCbCr(yPreview, CbPreview, CrPreview);
			(RPreview, GPreview, BPreview) = ColorSpaceConverter.convertYCbCrToRGB(yPreview, CbPreview, CrPreview);
		}

		/// <summary>
		/// Проверяет наличие изображения.
		/// </summary>
		/// <exception cref="Exception">Выбрасывается если отсутствует изображение на полотне.</exception>
		private void CheckImageIsNotNull()
		{
			if (Image == null)
			{
				throw new Exception("Отсутствует изображение на полотне.");
			}
		}

		#region Поля, свойства и функции, отвечающие за вывод отдельных каналов RGB или YCbCr
		/// <summary>
		/// Режим отображения. Если true - то RGB, иначе - YCbCr.
		/// </summary>
		private bool isRGB;

		/// <summary>
		/// Режим отображения. Если true - то RGB, иначе - YCbCr.
		/// </summary>
		public bool IsRGB
		{
			get { return isRGB; }
			set
			{
				isRGB = value;
				previewImage();
			}
		}

		/// <summary>
		/// Осуществляет предпросмотр изображения.
		/// </summary>
		public void previewImage()
		{
			if(isRGB)
			{
				previewImageByRGB();
			}
			else
			{
				previewImageByYCbCr();
			}
		}

		/// <summary>
		/// Отображать ли красный канал.
		/// </summary>
		private bool isRedActive;

		/// <summary>
		/// Отображать ли зеленый канал.
		/// </summary>
		private bool isGreenActive;

		/// <summary>
		/// Отображать ли синий канал.
		/// </summary>
		private bool isBlueActive;

		/// <summary>
		/// Отображать ли красный канал.
		/// </summary>
		public bool IsRedActive
		{
			get { return isRedActive; }

			set
			{
				isRedActive = value;
				previewImage();
			}
		}

		/// <summary>
		/// Отображать ли зеленый канал.
		/// </summary>
		public bool IsGreenActive
		{
			get { return isGreenActive; }

			set
			{
				isGreenActive = value;
				previewImage();
			}
		}

		/// <summary>
		/// Отображать ли синий канал.
		/// </summary>
		public bool IsBlueActive
		{
			get { return isBlueActive; }

			set
			{
				isBlueActive = value;
				previewImage();
			}
		}

		/// <summary>
		/// Обновляет изображение в соответствии с установенными массивами RGB и их активностью.
		/// </summary>
		/// <exception cref="Exception">Выбрасывается если image.Source не является WriteableBitmap.</exception>

		private void updateImageByRGB(byte[] red, byte[] green, byte[] blue)
		{
			WriteableBitmap source = Image.Source as WriteableBitmap;
			if (source == null)
			{
				throw new Exception("image.Source не является WriteableBitmap");
			}

			byte[] redBytes = IsRedActive ? red : new byte[red.Length];
			byte[] greenBytes = IsGreenActive ? green : new byte[green.Length];
			byte[] blueBytes = IsBlueActive ? blue : new byte[blue.Length];

			Image.Source = ImageHelper.composeBitmapByRGB(redBytes, greenBytes, blueBytes, source.PixelHeight, source.PixelWidth);
		}

		/// <summary>
		/// Отображать ли компоненту яркости.
		/// </summary>
		private bool isYActive;

		/// <summary>
		/// Отображать ли синюю цветоразностную компоненту.
		/// </summary>
		private bool isCbActive;

		/// <summary>
		/// Отображать ли красную цветоразностную компоненту.
		/// </summary>
		private bool isCrActive;

		/// <summary>
		/// Отображать ли компоненту яркости.
		/// </summary>
		public bool IsYActive
		{
			get { return isYActive; }

			set
			{
				isYActive = value;
				previewImage();
			}
		}

		/// <summary>
		/// Отображать ли синюю цветоразностную компоненту.
		/// </summary>
		public bool IsCbActive
		{
			get { return isCbActive; }

			set
			{
				isCbActive = value;
				previewImage();
			}
		}


		/// <summary>
		/// Отображать ли красную цветоразностную компоненту.
		/// </summary>
		public bool IsCrActive
		{
			get { return isCrActive; }

			set
			{
				isCrActive = value;
				previewImage();
			}
		}

		/// <summary>
		/// Обновляет изображение в соответствии с установенными массивами YCbCr и их активностью.
		/// </summary>
		private void updateImageByYCbCr(byte[] y, byte[] cb, byte[] cr)
		{
			WriteableBitmap bitmap = Image.GetWritableBitmap();

			byte[] emptyArray = null;
			int length = bitmap.PixelHeight * bitmap.PixelWidth;

			if (!isCbActive || !isCrActive)
			{
				emptyArray = new byte[length];
				for (int i = 0; i < length; i++)
				{
					emptyArray[i] = emptyCbCrValue;
				}
			}

			byte[] yBytes = IsYActive ? y : new byte[length];
			byte[] cbBytes = IsCbActive ? cb : emptyArray;
			byte[] crBytes = IsCrActive ? cr : emptyArray;

			Image.Source = ImageHelper.composeBitmapFromYCbCr(yBytes, cbBytes, crBytes, bitmap.PixelHeight, bitmap.PixelWidth);
		}
        #endregion

        #region Инвертирование цветов

		/// <summary>
		/// Инвертирует цвета.
		/// </summary>
        public void InvertColors()
		{
			CheckImageIsNotNull();

			ImageHelper.Invert(RPreview);
			ImageHelper.Invert(GPreview);
			ImageHelper.Invert(BPreview);
			previewImageByRGB();
		}
		#endregion

		#region Изменение яркости
		/// <summary>
		/// Величина смещения яркости.
		/// </summary>
		private int brightnessOffset;

		/// <summary>
		/// Изменяет яркость.
		/// </summary>
		public int BrightnessOffset
		{
			get
			{
				return brightnessOffset;
			}

			set
			{
				brightnessOffset = value;
				PreviewBrightnessChanging();
			}
		}


		#region Изменение насыщенности
		/// <summary>
		/// Модель представления параметра, отвечающего за насыщенность.
		/// </summary>
		public UpDownSliderVM<double> ColorfulnessVM { get; } = new UpDownSliderVM<double>("Коэфициент насыщенности", 1,  0.05, 0, 10);
		
		/// <summary>
		/// Обработчик изменения коэфициента насыщенности.
		/// </summary>
		private void ColorfulnessVM_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			if (e.PropertyName != nameof(UpDownSliderVM<double>.Value)) {
				return;
			}
			double coefficient = (sender as UpDownSliderVM<double>).Value;
			PreviewColorfullnesChanging(coefficient);
		}

		/// <summary>
		/// Предпросмотр изменения насыщенности.
		/// </summary>
		/// <param name="coefficient">Коэфициент насыщенности.</param>
		private void PreviewColorfullnesChanging(double coefficient)
		{
			int length = this.Red.Length;
			for(int i = 0; i < length; i++) {
				(double hue, double saturation, double value) = ColorToHSV(Red[i], Green[i], Blue[i]);
				(RPreview[i], GPreview[i], BPreview[i]) = ColorFromHSV(hue, saturation * coefficient, value);
			}
			previewImageByRGB();
		}

		/// <summary>
		/// Преобразует RGB в HSV.
		/// </summary>
		/// <param name="red">Красный.</param>
		/// <param name="green">Зеленый.</param>
		/// <param name="blue">Синий.</param>
		/// <returns></returns>
		public static (double hue, double saturation, double value) ColorToHSV(byte red, byte green, byte blue)
		{
			int max = Math.Max(red, Math.Max(green, blue));
			int min = Math.Min(red, Math.Min(green, blue));

			System.Drawing.Color color = System.Drawing.Color.FromArgb(red, green, blue);
			double hue = color.GetHue();
			double saturation = (max == 0) ? 0 : 1d - (1d * min / max);
			double value = max / 255d;
			return (hue, saturation, value);
		}

		/// <summary>
		/// Преобразует HSV в RGB.
		/// </summary>
		/// <param name="hue">Тон.</param>
		/// <param name="saturation">Насыщенность.</param>
		/// <param name="value">Яркость.</param>
		/// <returns></returns>
		public static (byte red, byte green, byte blue) ColorFromHSV(double hue, double saturation, double value)
		{
			int hi = Convert.ToInt32(Math.Floor(hue / 60)) % 6;
			double f = hue / 60 - Math.Floor(hue / 60);

			value = value * 255;
			byte v = ByteConverter.ToByte(value);
			byte p = ByteConverter.ToByte(value * (1 - saturation));
			byte q = ByteConverter.ToByte(value * (1 - f * saturation));
			byte t = ByteConverter.ToByte(value * (1 - (1 - f) * saturation));

			if (hi == 0)
				return (v, t, p);
			else if (hi == 1)
				return (q, v, p);
			else if (hi == 2)
				return (p, v, t);
			else if (hi == 3)
				return (p, q, v);
			else if (hi == 4)
				return (t, p, v);
			else
				return (v, p, q);
		}
		#endregion

		/// <summary>
		/// Предпросмотр изменения яркости.
		/// </summary>
		public void PreviewBrightnessChanging()
		{
			if (IsRGB)
			{
				PreviewBrightnessChangingByRGB(brightnessOffset);
			}
			else
			{
				PreviewBrightnessChangingByYCbCr(brightnessOffset);
			}
		}

		/// <summary>
		/// Предпросмотр изменения яркости на основе каналов YCbCr.
		/// </summary>
		/// <param name="offset">Смещение.</param>
		private void PreviewBrightnessChangingByYCbCr(int offset)
		{
			ImageHelper.shiftArrValues(yPreview, Y, offset);
			previewImageByYCbCr();
		}

		/// <summary>
		/// Предпросмотр изменения яркости на основе каналов RGB.
		/// </summary>
		/// <param name="offset">Смещение.</param>
		private void PreviewBrightnessChangingByRGB(int offset)
		{
			ImageHelper.shiftArrValues(RPreview, Red, offset);
			ImageHelper.shiftArrValues(GPreview, Green, offset);
			ImageHelper.shiftArrValues(BPreview, Blue, offset);

			previewImageByRGB();
		}
		#endregion

		#region Изменение контраста
		/// <summary>
		/// Коэфициент изменения контраста.
		/// </summary>
		private double contrastCoefficient;

		/// <summary>
		/// Коэфициент изменения контраста.
		/// </summary>
		public double ContrastCoefficient
		{
			get
			{
				return contrastCoefficient;
			}

			set
			{
				contrastCoefficient = value;
				PreviewContrastChanging();
			}
		}

		/// <summary>
		/// Предпросмотр изменения контраста.
		/// </summary>
		public void PreviewContrastChanging()
		{
			double coefficient = Math.Pow(1.125, contrastCoefficient);//contrastCoefficient > 0 ? contrastCoefficient : -1 / contrastCoefficient;
			ImageHelper.ChangeContrast(Y, out yPreview, coefficient);
			previewImageByYCbCr();
		}

		/// <summary>
		/// Отмена изменения. Пока не реализована.
		/// </summary>
		public void Cancel()
		{
		}
		#endregion

		#region Сжатие цветового диапазона
		/// <summary>
		/// Минимальное значение нового цветового диапазона.
		/// </summary>
		private byte minReducingValue;
		/// <summary>
		/// Максимальное значение нового цветового диапазона.
		/// </summary>
		private byte maxReducingValue;

		/// <summary>
		/// Сжимать ли цветовой диапазон синего канала.
		/// </summary>
		private bool isBlueReducing;

		/// <summary>
		/// Сжимать ли цветовой диапазон зеленого канала.
		/// </summary>
		private bool isGreenReducing;

		/// <summary>
		/// Сжимать ли цветовой диапазон красного канала.
		/// </summary>
		private bool isRedReducing;

		/// <summary>
		/// Минимальное значение нового цветового диапазона.
		/// </summary>
		public byte MinReducingValue
		{
			get
			{
				return minReducingValue;
			}

			set
			{
				minReducingValue = value;
				PreviewColorRangeReducing();
			}
		}

		/// <summary>
		/// Максимальное значение нового цветового диапазона.
		/// </summary>
		public byte MaxReducingValue
		{
			get
			{
				return maxReducingValue;
			}

			set
			{
				maxReducingValue = value;
				PreviewColorRangeReducing();
			}
		}

		/// <summary>
		/// Сжимать ли красный диапазон зеленого канала.
		/// </summary>
		public bool IsRedReducing
		{
			get { return isRedReducing; }

			set
			{
				isRedReducing = value;
				PreviewColorRangeReducing();
			}
		}

		/// <summary>
		/// Сжимать ли цветовой диапазон зеленого канала.
		/// </summary>
		public bool IsGreenReducing
		{
			get { return isGreenReducing; }

			set
			{
				isGreenReducing = value;
				PreviewColorRangeReducing();
			}
		}

		/// <summary>
		/// Сжимать ли цветовой диапазон синего канала.
		/// </summary>
		public bool IsBlueReducing
		{
			get { return isBlueReducing; }

			set
			{
				isBlueReducing = value;
				PreviewColorRangeReducing();
			}
		}

		/// <summary>
		/// Предпросмотр изменения цветового диапазона.
		/// </summary>
		public void PreviewColorRangeReducing()
		{
			ReduceOrCopy(IsRedReducing, Red, RPreview);
			ReduceOrCopy(IsGreenReducing, Green, GPreview);
			ReduceOrCopy(isBlueReducing, Blue, BPreview);

			previewImageByRGB();
		}

		/// <summary>
		/// Сжимает цветовой диапазон либо просто копирует исходный массив.
		/// </summary>
		/// <param name="isReducing">Сжимать ли цветовой диапазон.</param>
		/// <param name="original">Исходный массив.</param>
		/// <param name="preview">Массив для предпросмотра.</param>
		private void ReduceOrCopy(bool isReducing, byte[] original, byte[] preview)
		{
			byte min = minReducingValue;
			byte max = maxReducingValue;
			if (min == max)
			{
				return;
			}
			SwapIfInversed(ref min, ref max);

			if (isReducing)
			{
				ImageHelper.ReduceRange(original, min, max, preview);
			}
			else
			{
				original.CopyTo(preview, 0);
			}
		}

		/// <summary>
		/// Упорядочивает значения минимума и максимума.
		/// </summary>
		/// <param name="min">Минимум.</param>
		/// <param name="max">Максимум.</param>
		private static void SwapIfInversed(ref byte min, ref byte max)
		{
			if (min > max)
			{
				byte swap = min;
				min = max;
				max = swap;
			}
		}
		#endregion

		#region Выделение по диапазону значений компонента
		/// <summary>
		/// Тип компонента, по которому происходит выделение.
		/// </summary>
		ImageComponents componentToSelectBrightness;

		/// <summary>
		/// Тип компонента, по которому происходит выделение.
		/// </summary>
		public ImageComponents CompontentToSelectBrightness
		{
			get
			{
				return componentToSelectBrightness;
			}

			set
			{
				componentToSelectBrightness = value;
                selectByBrightnessLevel();

            }
		}

		/// <summary>
		/// Левая граница диапазона выделения.
		/// </summary>
		private byte selectionLevelLeftBrightness;

		/// <summary>
		/// Левая граница диапазона выделения.
		/// </summary>
		public byte SelectionLevelLeftBrightness
		{
			get { return selectionLevelLeftBrightness; }

			set
			{
				selectionLevelLeftBrightness = value;
                selectByBrightnessLevel();

            }
		}

		/// <summary>
		/// Правая граница диапазона выделения.
		/// </summary>
		private byte selectionLevelRightBrightness;

		/// <summary>
		/// Правая граница диапазона выделения.
		/// </summary>
		public byte SelectionLevelRightBrightness
		{
			get { return selectionLevelRightBrightness; }

			set
			{
				selectionLevelRightBrightness = value;
                selectByBrightnessLevel();
            }
		}

		/// <summary>
		/// Выделяет изображение по попаданию в диапазон того или иного компонента.
		/// </summary>
        private void selectByBrightnessLevel()
        {
            selectByLevel(selectionLevelLeftBrightness, selectionLevelRightBrightness, componentToSelectBrightness);
        }

		/// <summary>
		/// Выделяет изображение по уровню канала.
		/// </summary>
		/// <param name="leftLevel">Левая граница диапазона выделения.</param>
		/// <param name="rightLevel">Правая граница диапазона выделения.</param>
		/// <param name="component">Тип компонента, по значениям которого производится выдеделние.</param>
		private void selectByLevel(byte leftLevel, byte rightLevel, ImageComponents component)
        {
            byte min = leftLevel;
            byte max = rightLevel;
            SwapIfInversed(ref min, ref max);
            switch (component)
            {
                case ImageComponents.Y:
                case ImageComponents.Cb:
                case ImageComponents.Cr:
                    ImageHelper.SelectByYCbCr(yPreview, CbPreview, CrPreview, Y, Cb, Cr, min, max, component);
                    previewImageByYCbCr();
                    break;

                case ImageComponents.Red:
                case ImageComponents.Green:
                case ImageComponents.Blue:
                    ImageHelper.SelectByRGB(RPreview, GPreview, BPreview, Red, Green, Blue, min, max, component);
                    previewImageByRGB();
                    break;
            }
        }

		#endregion

		#region Удаление шума
		/// <summary>
		/// Удаляет шум.
		/// </summary>
		/// <exception cref="Exception">Выбрасывается если произошла ошибка при преобразовании.</exception>
		public void Clarity()
		{
			try
			{
				CheckImageIsNotNull();
				ImageHelper.Convol(Image, PredefinedKernels.Sharpen, RPreview, GPreview, BPreview);
			}
			catch (Exception ex)
			{
				throw new Exception("Ошибка преобразования: " + ex.Message);
			}
			finally
			{
				previewImageByRGB();
			}
		}
		#endregion

		#region Выделение границ
		/// <summary>
		/// Выделяет границы
		/// </summary>
		/// <exception cref="Exception">Выбрасывается если произошла ошибка при преобразовании.</exception>
		public void Borders()
		{
			try
			{
				CheckImageIsNotNull();
				ImageHelper.Convol(Image, PredefinedKernels.EdgeDetect, RPreview, GPreview, BPreview);
			}
			catch (Exception ex)
			{
				throw new Exception("Ошибка преобразования: " + ex.Message);
			}
			finally
			{
				previewImageByRGB();
			}
			//throw new NotImplementedException();
		}
		#endregion

		#region Размытие
		/// <summary>
		/// Размывает изображение.
		/// </summary>
		/// <exception cref="Exception">Выбрасывается если произошла ошибка при преобразовании.</exception>
		public void Blur()
		{
			try
			{
				CheckImageIsNotNull();
				ImageHelper.Convol(Image, PredefinedKernels.BoxBlur, RPreview, GPreview, BPreview);
			}
			catch (Exception ex)
			{
				throw new Exception("Ошибка преобразования: " + ex.Message);
			}
			finally
			{
				previewImageByRGB();
			}
		}
		#endregion

		#region Снижение резкости
		/// <summary>
		/// Снижает резкость.
		/// </summary>
		/// <exception cref="Exception">Выбрасывается если произошла ошибка при преобразовании.</exception>
		public void UnSharpening()
        {
            try
            {
                CheckImageIsNotNull();
                ImageHelper.Convol(Image, PredefinedKernels.Unsharpen, RPreview, GPreview, BPreview);
            }
            catch (Exception ex)
            {
                throw new Exception("Ошибка преобразования: " + ex.Message);
            }
            finally
            {
                previewImageByRGB();
            }
        }
		#endregion

		#region Гауссово размытие
		/// <summary>
		/// Осуществляет гауссово размытие.
		/// </summary>
		/// <exception cref="Exception">Выбрасывается если произошла ошибка при преобразовании.</exception>
		public void GaussianBluring()
        {
            try
            {
                CheckImageIsNotNull();
                ImageHelper.Convol(Image, PredefinedKernels.GaussianBlur, RPreview, GPreview, BPreview);
            }
            catch (Exception ex)
            {
                throw new Exception("Ошибка преобразования: " + ex.Message);
            }
            finally
            {
                previewImageByRGB();
            }
        }
        #endregion

		#region Изменение насыщенности
		/// <summary>
		/// Изменяется ли насыенность красного канала.
		/// </summary>
		private bool isRedChangeLevel;

		/// <summary>
		/// Изменяется ли насыенность зеленого канала.
		/// </summary>
		private bool isGreenChangeLevel;

		/// <summary>
		/// Изменяется ли насыенность синего канала.
		/// </summary>
		private bool isBlueChangeLevel;

		/// <summary>
		/// Изменяется ли насыенность красного канала.
		/// </summary>
		public bool IsRedChangeLevel
		{
			get
			{
				return isRedChangeLevel;
			}

			set
			{
				isRedChangeLevel = value;
				PreviewLevelChanging();
			}
		}

		/// <summary>
		/// Изменяется ли насыенность зеленого канала.
		/// </summary>
		public bool IsGreenChangeLevel
		{
			get
			{
				return isGreenChangeLevel;
			}

			set
			{
				isGreenChangeLevel = value;
				PreviewLevelChanging();
			}
		}

		/// <summary>
		/// Изменяется ли насыенность синего канала.
		/// </summary>
		public bool IsBlueChangeLevel
		{
			get
			{
				return isBlueChangeLevel;
			}

			set
			{
				isBlueChangeLevel = value;
				PreviewLevelChanging();
			}
		}

		/// <summary>
		/// Изменение насыщенности.
		/// </summary>
		public int offset;

		/// <summary>
		/// Изменение насыщенности.
		/// </summary>
		public int Offset
		{
			get
			{
				return offset;
			}

			set
			{
				offset = value;
				PreviewLevelChanging();
			}
		}

		/// <summary>
		/// Предпросмотр изменения насыщенности.
		/// </summary>
		private void PreviewLevelChanging()
		{
			if (isRedChangeLevel)
			{
				ImageHelper.shiftArrValues(RPreview, Red, offset);
			}
			else
			{
				Red.CopyTo(RPreview, 0);
			}
			if (isGreenChangeLevel)
			{
				ImageHelper.shiftArrValues(GPreview, Green, offset);
			}
			else
			{
				Green.CopyTo(GPreview, 0);
			}
			if (isBlueChangeLevel)
			{
				ImageHelper.shiftArrValues(BPreview, Blue, offset);
			}
			else
			{
				Blue.CopyTo(BPreview, 0);
			}

			previewImageByRGB();
		}
		#endregion

        #region Масштаб
		/// <summary>
		/// Модель представления для значения масштаба.
		/// </summary>
        public UpDownSliderVM<int> ScaleViewModel { get; } = new UpDownSliderVM<int>(
            title: "Масштаб",
            defaultValue: 100,
            increment: 1,
            minimum: 1,
            maximum: 800
        );

		/// <summary>
		/// Обработчик изменения масштаба.
		/// </summary>
		private void ScaleViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            double scale = ScaleViewModel.Value / 100d;

            Image.Width = Image.Source.Width * scale;
            Image.Height = Image.Source.Height * scale;

            setPaneSize(Image.Width, Image.Height);

            rowHeight = rowHeightPixel * scale;
            colWidth = colWidthPixel * scale;
            UpdateGridIfVisible();
            updateRulersIfVisible();
        }

        #endregion

        #region Линейка и сетка
		/// <summary>
		/// Высота линейки.
		/// </summary>
        double rulerHeight;

		/// <summary>
		/// Ширина линейки.
		/// </summary>
		double rulerWidth;

		/// <summary>
		/// Высота сетки.
		/// </summary>
		double gridHeight;

		/// <summary>
		/// Ширина сетки.
		/// </summary>
		double gridWidth;

		/// <summary>
		/// Стандартная высота строки сетки.
		/// </summary>
		const double rowHeightPixel = 50;

		/// <summary>
		/// Высота строки строки сетки в пикселях.
		/// </summary>
		double rowHeight = rowHeightPixel;

		/// <summary>
		/// Стандартная ширина столбца строки сетки.
		/// </summary>
		const double colWidthPixel = 50;

		/// <summary>
		/// Ширина столбца сетки в пикселях.
		/// </summary>
		double colWidth = colWidthPixel;

		/// <summary>
		/// Колчиество маленьких штрихов между большими.
		/// </summary>
        const int strokesPerCell = 5;

		/// <summary>
		/// Показывает линейку.
		/// </summary>
		public void ShowRuler()
		{
			if(topRuler.Visibility == Visibility.Collapsed)
            {
                topRuler.Visibility =
                leftRuler.Visibility = Visibility.Visible;
                updateRulersIfVisible();
            }
            else
			{
				topRuler.Visibility =
				leftRuler.Visibility = Visibility.Collapsed;
			}
		}

		/// <summary>
		/// Обновляет линейку, если она видима.
		/// </summary>
        private void updateRulersIfVisible()
        {
            if (rulerHeight != imageCanvas.Height && leftRuler.Visibility == Visibility.Visible)
            {
                rulerHeight = imageCanvas.Height;
                UpdateLeftRuler();
            }

            if (rulerWidth != imageCanvas.Width && topRuler.Visibility == Visibility.Visible)
            {
                rulerWidth = imageCanvas.Width;
                UpdateTopRuler();
            }
        }

		/// <summary>
		/// Показывает сетку.
		/// </summary>
        public void ShowGrid()
		{
			grid.ShowGridLines = !grid.ShowGridLines;
            UpdateGridIfVisible();
        }

		/// <summary>
		/// Обновляет сетку, если она видима.
		/// </summary>
        private void UpdateGridIfVisible()
        {
			if (grid.ShowGridLines)
            {
                if (gridHeight != imageCanvas.Height)
                {
                    gridHeight = imageCanvas.Height;
                    UpdateGridRows();
                }

                if (gridWidth != imageCanvas.Width)
                {
                    gridWidth = imageCanvas.Width;
                    UpdateGridColumns();
                }
            }
        }

		/// <summary>
		/// Обновляет столбцы сетки.
		/// </summary>
        private void UpdateGridColumns()
		{
			removeColumns(grid);
			grid.Width = gridWidth;
			for (double currentWidth = 0; currentWidth < gridWidth; currentWidth = grid.ColumnDefinitions.Count * colWidth)
			{
                var column = new ColumnDefinition
                {
                    Width = new GridLength(colWidth)
                };
                grid.ColumnDefinitions.Add(column);
			}
		}

		/// <summary>
		/// Обновляет строки сетки.
		/// </summary>
		private void UpdateGridRows()
        {
			removeRows(grid);
			grid.Height = gridHeight;
			for (double currentHeight = 0; currentHeight < gridHeight; currentHeight = grid.RowDefinitions.Count * rowHeight)
				{
					var row = new RowDefinition();
					row.Height = new GridLength(rowHeight);
					grid.RowDefinitions.Add(row);
				}
		}

		/// <summary>
		/// Обновляет верхнюю линейку.
		/// </summary>
		private void UpdateTopRuler()
        {
            removeColumns(topRuler);

            topRuler.Width = rulerWidth;
            int currentColumn = 0;
            for (double currentWidth = 0; currentWidth < rulerWidth; currentWidth = topRuler.ColumnDefinitions.Count * colWidth)
            {
                var topRulerColumn = new ColumnDefinition
                {
                    Width = new GridLength(colWidth)
                };
                topRuler.ColumnDefinitions.Add(topRulerColumn);

                //Создаем и добавляем надпись с количеством пикселей
                TextBlock label = new TextBlock
                {
                    Margin = new Thickness(2, 0, 0, 0),
                    FontSize = 9,
                    Text = (currentColumn * colWidthPixel).ToString()
                };
                AddElement(topRuler, label, 0, currentColumn);

                //Создаем и добавляем большие штрихи
                Border border = new Border
                {
                    Width = 1,
                    Margin = new Thickness(-0.5, 0, 0, 0),
                    HorizontalAlignment = HorizontalAlignment.Left,
                    Background = Brushes.Gray
                };
                AddElement(topRuler, border, 0, currentColumn);

                Border border2 = new Border
                {
                    Width = 1,
                    Margin = new Thickness(-0.5, 0, 0, 0),
                    HorizontalAlignment = HorizontalAlignment.Left,
                    Background = Brushes.Gray
                };
                AddElement(topRuler, border2, 1, currentColumn);


                //Создаем и добавляем маленькие штрихи
                Grid strokesGrid = new Grid();
                for (int cNum = 0; cNum < strokesPerCell; cNum++)
                {
                    var colStroke = new ColumnDefinition();
                    strokesGrid.ColumnDefinitions.Add(colStroke);

                    Border stroke = new Border
                    {
                        Width = 0.5,
                        Margin = new Thickness(-0.25, 0, 0, 0),
                        HorizontalAlignment = HorizontalAlignment.Left,
                        Background = Brushes.Gray
                    };
                    AddElement(strokesGrid, stroke, 0, cNum);
                }
                AddElement(topRuler, strokesGrid, 1, currentColumn);

                currentColumn++;
            }
        }

		/// <summary>
		/// Обновляет левую линейку.
		/// </summary>
        private void UpdateLeftRuler()
		{
			removeRows(leftRuler);

			leftRuler.Height = rulerHeight;
			int currentRow = 0;
            for (double currentHeight = 0; currentHeight < rulerHeight; currentHeight = leftRuler.RowDefinitions.Count * rowHeight)
			{
                var leftRulerRow = new RowDefinition
                {
                    Height = new GridLength(rowHeight)
                };
                leftRuler.RowDefinitions.Add(leftRulerRow);

				var transform = new TransformGroup();
				transform.Children.Add(new RotateTransform(-90));
                TextBlock label = new TextBlock
                {
                    Text = (currentRow * rowHeightPixel).ToString(),
                    FontSize = 9,
                    TextWrapping = TextWrapping.Wrap,
                    TextAlignment = TextAlignment.Right,
                    HorizontalAlignment = HorizontalAlignment.Right,
                    VerticalAlignment = VerticalAlignment.Top,
                    RenderTransform = transform,
                    RenderTransformOrigin = new System.Windows.Point(0, 0),
                    Margin = new Thickness(0, 32, 0, 0),
                    Width = 30
                };
                AddElement(leftRuler, label, currentRow, 0);
				//Почему-то после поворота TextBlock обрезается правая часть с текстом, 
				//Изначально не помещавшаяся в колонку. Хак: устанавливаем ColumnSpan, 
				//чтобы не обрезался текст.
				Grid.SetColumnSpan(label, 2);


                Border border = new Border
                {
                    Height = 1,
                    Margin = new Thickness(0, -0.5, 0, 0),
                    VerticalAlignment = VerticalAlignment.Top,
                    Background = Brushes.Gray
                };
                AddElement(leftRuler, border, currentRow, 0);

                Border border2 = new Border
                {
                    Height = 1,
                    Margin = new Thickness(0, -0.5, 0, 0),
                    VerticalAlignment = VerticalAlignment.Top,
                    Background = Brushes.Gray
                };

				AddElement(leftRuler, border2, currentRow, 1);

				Grid strokesGrid = new Grid();
				for (int rNum = 0; rNum < strokesPerCell; rNum++)
				{
					var rowStroke = new RowDefinition();
					strokesGrid.RowDefinitions.Add(rowStroke);
                    Border stroke = new Border
                    {
                        Height = 0.5,
                        Margin = new Thickness(0, -0.25, 0, 0),
                        VerticalAlignment = VerticalAlignment.Top,
                        Background = Brushes.Gray
                    };

                    AddElement(strokesGrid, stroke, rNum, 0);
				}
				AddElement(leftRuler, strokesGrid, currentRow, 1);
				currentRow++;
			}
		}

		/// <summary>
		/// Удаляет столбцы в сетке.
		/// </summary>
		/// <param name="grid">Сетка.</param>
		private void removeColumns(Grid grid)
		{
            grid.ColumnDefinitions.Clear();
            grid.Children.Clear();
        }

		/// <summary>
		/// Удаляет строки в сетке.
		/// </summary>
		/// <param name="grid">Сетка.</param>
		private void removeRows(Grid grid)
		{
            grid.RowDefinitions.Clear();
            grid.Children.Clear();
		}

		/// <summary>
		/// Добавляет элемент в таблицу.
		/// </summary>
		/// <param name="grid">Таблица.</param>
		/// <param name="element">Элемент, который нужно добавить.</param>
		/// <param name="row">Строка, в которую нужно добавить элемент.</param>
		/// <param name="column">Столбец, в который нужно добавить элемент.</param>
		private void AddElement(Grid grid, UIElement element, int row, int column)
		{
			grid.Children.Add(element);
			Grid.SetColumn(element, column);
			Grid.SetRow(element, row);
		}
		#endregion

	}
}
