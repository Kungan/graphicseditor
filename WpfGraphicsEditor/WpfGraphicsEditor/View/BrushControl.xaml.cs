﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfGraphicsEditor
{
	/// <summary>
	/// Логика взаимодействия для BrushControl.xaml
	/// </summary>
	/// 
	public partial class BrushControl : UserControl
	{
		public BrushControl()
		{
			InitializeComponent();	
		}

		public void AddColorCommand_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			GradientStop gradientStop = (DataContext as BrushControlVM).AddColor();
			Slider slider = new Slider();

			Binding binding = new Binding(nameof(gradientStop.Offset));
			binding.Source = gradientStop;
			slider.SetBinding(Slider.ValueProperty, binding);

			StopColorSliders.Children.Add(slider);
		}
	}
}
