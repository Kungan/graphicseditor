﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfGraphicsEditor
{
	/// <summary>
	/// Логика взаимодействия для GradientPreview.xaml
	/// </summary>
	public partial class GradientPreview : Window
	{
		private Pane pane;
		private BindingSource bindingSource = new BindingSource();

		public GradientPreview(Pane pane)
		{
			InitializeComponent();
			this.pane = pane;
			WriteableBitmap bitmap = pane.Image.Source as WriteableBitmap;

			try
			{
				FillDataGridView(pane, bitmap.PixelWidth);
			}
			catch (Exception e)
			{
				System.Windows.MessageBox.Show("Произошла ошибка:\n" + e.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
				this.Close();
			}
		}

		private void FillDataGridView(Pane pane, int width)
		{
			byte[] r = pane.RPreview;
			byte[] g = pane.GPreview;
			byte[] b = pane.BPreview;
			GradientsField pixelGradients = new GradientsField(width, r, g, b);
			this.DataContext = pixelGradients;
			PixelGradient[,] field = pixelGradients.Field;
			int height = field.GetLength(0);

			var dataGridView = host.Child as DataGridView;
			dataGridView.Columns.Clear();

			var cellTemplate = new DataGridViewTextBoxCell();
			for (int w = 0; w < width; ++w)
			{
				var column = new DataGridViewColumn();
				column.HeaderText = w.ToString();
				column.Width = 40;
				//Иначе при большом количестве колонок появляется сообщение о слишком большой сумме FillWeight
				column.FillWeight = 0.25f;
				column.CellTemplate = cellTemplate;
				dataGridView.Columns.Add(column);
			}

			dataGridView.RowCount = height;
			dataGridView.RowHeadersWidth = 60;
			float[] gradients;
			int length;
			int directionIndex = 0;
			int i, j, k;
			var arrows = new char[] { '↖', '↑', '↗', '→', '↘', '↓', '↓', '←' };
			for (i = 0; i < height; ++i)
			{
				DataGridViewRow row = dataGridView.Rows[i];
				row.Height = 40;
				row.HeaderCell.Value = i.ToString();
				for (j = 0; j < width; ++j)
				{
					DataGridViewCell cell = row.Cells[j];
					gradients = field[i, j].Gradients;
					length = gradients.Length;
					for(k = 0; k < length; k++)
					{
						if(gradients[k] > gradients[directionIndex])
						{
							directionIndex = k;
						}
					}

					cell.Value = arrows[directionIndex];

					//cell.Style.BackColor = System.Drawing.Color.FromArgb(pixel.R, pixel.G, pixel.B);
				}
			}
		}
		
		private void DataGridView_SelectionChanged(object sender, EventArgs e)
		{
			DataGridViewCell cell = (sender as DataGridView).CurrentCell;
			int rowIndex = cell.RowIndex;
			int columnIndex = cell.ColumnIndex;
			if (rowIndex < 0)
			{
				return;
			}
			(this.DataContext as GradientsField).CurrentRowColumn = new Tuple<int, int>(rowIndex, columnIndex);
		}
	}
}
