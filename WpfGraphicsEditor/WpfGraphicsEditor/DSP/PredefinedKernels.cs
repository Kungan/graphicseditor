﻿
namespace Convolution.DSP
{
	/// <summary>
	/// Хранит предопределенные ядра свертки, предназначенные для достижения тех или иных эффектов.
	/// </summary>
    static class PredefinedKernels
    {
		/// <summary>
		/// Снижение резкости.
		/// </summary>
        public readonly static double[,] Unsharpen = 
		{
			{ 3, -14, 3 },
			{ 0, 0, 0 }, 
			{ -3, 14, -3 }
		};

		/// <summary>
		/// Удаление шума.
		/// </summary>
        public readonly static double[,] Sharpen = 
		{ 
			{ -1, -1, -1 }, 
			{ -1, 16, -1 },
			{ -1, -1, -1 }
		};

		/// <summary>
		/// Обнружение границ. 
		/// </summary>
        public readonly static double[,] EdgeDetect =
		{ 
			{ -1, -1, -1 }, 
			{ 0, 0, 0 }, 
			{ 1, 1, 1 }
		};

		/// <summary>
		/// Размытие.
		/// </summary>
        public readonly static double[,] BoxBlur =
		{
			{ 1, 1, 1 },
			{ 1, 1, 1 },
			{ 1, 1, 1 }
		};

		/// <summary>
		/// Гауссово размытие.
		/// </summary>
        public readonly static double[,] GaussianBlur = 
		{
			{ 1, 2, 3, 2, 1 },
			{ 2, 4, 5, 4, 2 },
			{ 3, 5, 6, 5, 3 },
			{ 2, 4, 5, 4, 2 },
			{ 1, 2, 3, 2, 1 }
		};
    }
}