﻿using System.Drawing;
using System.Drawing.Imaging;

namespace Convolution.DSP
{
	/// <summary>
	/// Используется для конвертации битовой карты изображения и массива байт.
	/// </summary>
    public static class BitmapBytes
    {
		/// <summary>
		/// Преобразует битовую карту в массив баайт.
		/// </summary>
		/// <param name="input">Битвая карта.</param>
		/// <returns>Массив байт.</returns>
		public static byte[] GetBytes(Bitmap input)
        {
            int bytesCount = input.Width * input.Height * 3;
            BitmapData inputData = input.LockBits(
                new Rectangle(0, 0, input.Width, input.Height), 
                ImageLockMode.ReadOnly, 
                PixelFormat.Format24bppRgb);

            byte[] output = new byte[bytesCount];
            System.Runtime.InteropServices.Marshal.Copy(inputData.Scan0, output, 0, bytesCount);
            input.UnlockBits(inputData);
            return output;
        }

		/// <summary>
		/// Преобразует массив байт в битовую карту изображения.
		/// </summary>
		/// <param name="input">Массив байт. Его длина должна быть кратна 3
		/// (т. к. на один пиксель приходится по одному байту на каждый канал RGB.</param>
		/// <param name="width">Ширина изображения.</param>
		/// <param name="height">Высота изображени.</param>
		/// <returns>Битовая карта изображения.</returns>
        public static Bitmap GetBitmap(byte[] input, int width, int height)
        {
            if (input.Length % 3 != 0) return null;
            Bitmap output = new Bitmap(width, height);
            BitmapData outputData = output.LockBits(
                new Rectangle(0, 0, width, height), 
                ImageLockMode.ReadWrite, 
                PixelFormat.Format24bppRgb);

            System.Runtime.InteropServices.Marshal.Copy(input, 0, outputData.Scan0, input.Length);
            output.UnlockBits(outputData);
            return output;
        }
    }
}