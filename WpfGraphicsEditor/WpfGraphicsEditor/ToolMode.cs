﻿using System;

namespace WpfGraphicsEditor
{
	/// <summary>
	/// Режимы рисования.
	/// </summary>
	public enum ToolMode
	{
		/// <summary>
		/// Линия.
		/// </summary>
		Line,

		/// <summary>
		/// Перо.
		/// </summary>
		Ink,

		/// <summary>
		/// Прямоугольник.
		/// </summary>
		Rectangle,

		/// <summary>
		/// Полилиния.
		/// </summary>
		Polyline,

		/// <summary>
		/// Полигон.
		/// </summary>
		Polygon,

		/// <summary>
		/// Эллипс.
		/// </summary>
		Ellipse,

		/// <summary>
		/// Заливка.
		/// </summary>
		Filling,

		/// <summary>
		/// Ластик.
		/// </summary>
		Eraser,

		/// <summary>
		/// Обрезка.
		/// </summary>
		Clip,

		/// <summary>
		/// Выбор.
		/// </summary>
		Select
	}
}