﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace WpfGraphicsEditor.Model
{
	/// <summary>
	/// Класс, приводящий значение к типу byte.
	/// Если значение не укладывается в диапазон значений byte, то назначается
	/// минимальное или максимальное значение byte в зависимости от того, 
	/// лежит ли значение выше или ниже диапазона.
	/// </summary>
	static class ByteConverter
    {
		/// <summary>
		/// Приводит значение типа int к byte.
		/// </summary>
		/// <param name="value">Значение имеюее тип int.</param>
		/// <returns>Конвертированное значение в формате byte.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static byte ToByte(int value)
        {
            return 
                value > byte.MaxValue ? byte.MaxValue
					: value < byte.MinValue ? byte.MinValue
						: (byte)value;
        }

		/// <summary>
		/// Приводит значение типа double к byte.
		/// </summary>
		/// <param name="value">Значение имеюее тип int.</param>
		/// <returns>Конвертированное значение в формате byte.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static byte ToByte(double value)
		{
			return
				value > byte.MaxValue ? byte.MaxValue
					: value < byte.MinValue ? byte.MinValue
						: (byte)value;
		}
    }
}