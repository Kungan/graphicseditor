﻿using OpenCvSharp;
using OpenCvSharp.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace WpfGraphicsEditor.Model
{
	/// <summary>
	/// Класс, представляющий собой диапазон контуров, ограниченный пороговыми значениями яркости и длины контура.
	/// </summary>
    class ContourRange
    {
		/// <summary>
		/// Конструктор.
		/// </summary>
		/// <param name="channel">Канал изображения, для которого производится поиск контуров.</param>
		/// <param name="color">Цвет отображения контуров.</param>
		/// <param name="thickness">Толщина отображения контуров.</param>
		/// <param name="lowThreshold">Нижний порог яркости, начиная с которого ведется поиск контуров.</param>
		/// <param name="highThreshold">Верхний порог яркости, начиная с которого ведется поиск контуров.</param>
		/// <param name="minLength">Минимальная длина искомых контуров.</param>
		/// <param name="maxLength">Максимальная длина искомых контуров.</param>
		public ContourRange(Mat channel, Color color, int thickness, int lowThreshold, int highThreshold, int minLength, int maxLength)
        {
            this.channel = channel;
            this.isActive = true;
            this.color = color;
            this.thickness = thickness;
            this.lowThreshold = lowThreshold;
            this.highThreshold = highThreshold;
            this.minLength = minLength;
            this.maxLength = maxLength;

            refresh();
        }

		/// <summary>
		/// Активность.
		/// </summary>
        public bool IsActive
        {
            get => isActive;
            set
            {
                isActive = value;
                OnChanged();
            }
        }

		/// <summary>
		/// Цвет контуров.
		/// </summary>
		public Color Color
        {
            get => color;
            set
            {
                color = value;
                OnChanged();
            }
        }

		/// <summary>
		/// Толщина контуров.
		/// </summary>
        public int Thickness
        {
            get => thickness;
            set
            {
                thickness = value;
                OnChanged();
            }
        }

		/// <summary>
		/// Нижний порог яркости.
		/// </summary>
        public double LowThreshold
        {
            get => lowThreshold;
            set
            {
                lowThreshold = value;
                refresh();
                OnChanged();
            }
        }

		/// <summary>
		/// Верхний порог яркости.
		/// </summary>
        public double HighThreshold
        {
            get => highThreshold;
            set
            {
                highThreshold = value;
                refresh();
                OnChanged();
            }
        }

		/// <summary>
		/// Минимальная длина контуров.
		/// </summary>
        public int MinLength {
            get => minLength;
            set
            {
                minLength = value;
                FilterContours();
                OnChanged();
            }
        }

		/// <summary>
		/// Максимальная длина контуров.
		/// </summary>
		public int MaxLength {
            get => maxLength;
            set
            {
                maxLength = value;
                FilterContours();
                OnChanged();
            }
        }

		/// <summary>
		/// Обновляет контуры.
		/// </summary>
        private void refresh()
        {
            FindContours();
            FilterContours();
        }

		/// <summary>
		/// Сообщает об изменении диапазона контуров.
		/// </summary>
        public event EventHandler Changed;

		/// <summary>
		/// Рисует контуры на изображении.
		/// </summary>
		/// <param name="background">Фон.</param>
        public void DrawContour(Mat background)
        {
            Cv2.DrawContours(
                image: background,
                contours: filtredContours,
                contourIdx: -1,
                color: new Scalar(color.B, color.G, color.R),
                thickness: thickness,
                lineType: LineTypes.Link8,
                hierarchy: null,
                maxLevel: int.MaxValue
            );
        }

		/// <summary>
		/// Метод, находящий контуры на изображении.
		/// </summary>
        private void FindContours()
        {
            Mat truncatedChannel = truncateBrightness(lowThreshold, highThreshold);

            Cv2.Canny(
                src: truncatedChannel,
                edges: truncatedChannel,
                threshold1: lowThreshold - 1,
                threshold2: lowThreshold,
                apertureSize: 5
            );

            Cv2.FindContours(
                image: truncatedChannel,
                contours: out contours,
                hierarchy: out HierarchyIndex[] _,
                mode: RetrievalModes.List,
                method: ContourApproximationModes.ApproxSimple
            );
        }

		/// <summary>
		/// Метод делает черными пиксели изображения за пределами переданного диапазона яркости.
		/// Необходимо для того, чтобы находить контуры только в заданном диапазоне яркости.
		/// </summary>
		/// <param name="minimum">Минимум яркости для отсечения.</param>
		/// <param name="maximum">Максимум яркости для отсечения.</param>
		/// <returns></returns>
		private Mat truncateBrightness(double minimum, double maximum)
        {
            Mat processedChannel = new Mat();

            Cv2.Threshold(
                src: channel,
                dst: processedChannel,
                thresh: minimum,
                maxval: thresholdMaxVal,
                type: ThresholdTypes.Tozero
            );

            Cv2.Threshold(
                src: processedChannel,
                dst: processedChannel,
                thresh: maximum,
                maxval: thresholdMaxVal,
                type: ThresholdTypes.TozeroInv
            );

            return processedChannel;
        }


		/// <summary>
		/// Фильтрует контуры по длине. Отфильтрованные контуры хранятся в массиве filtredContours.
		/// <exception cref="Exception">Выбрасывается если предварительно не запускался поиск контуров.</exception>
		/// </summary>
		private void FilterContours()
        {
            if(contours == null)
            {
                throw new Exception("Контура отсутствуют. Сначала необходимо запустить поиск контуров.");
            }

            var filtredContoursList = new List<Point[]>();

            foreach (Point[] contour in contours)
            {
                double contourLength = Cv2.ArcLength(contour, true);
                if (minLength <= contourLength && contourLength <= maxLength)
                {
                    filtredContoursList.Add(contour);
                }
            }

            filtredContours = filtredContoursList.ToArray();
        }


        private const double thresholdMaxVal = 255;

		/// <summary>
		/// Канал изображения, для которого ищутся контуры.
		/// </summary>
        private Mat channel;

		/// <summary>
		/// Все контуры, найденные в заданном диапазоне яркости.
		/// </summary>
        private Point[][] contours;

		/// <summary>
		/// Все контуры, найденные в заданном диапазоне яркости и отфильтрованные по длине.
		/// </summary>
		private Point[][] filtredContours;

		/// <summary>
		/// Признак активности диапазона контуров.
		/// </summary>
        private bool isActive;

		/// <summary>
		/// Цвет отображения контуров.
		/// </summary>
        private Color color;

		/// <summary>
		/// Толщина отображаемых контуров.
		/// </summary>
        private int thickness;

		/// <summary>
		/// Нижний порог яркости изображения, начиная с которого ищутся контуры.
		/// </summary>
        private double lowThreshold;

		/// <summary>
		/// Верхний порог яркости изображения, начиная с которого ищутся контуры.
		/// </summary>
		private double highThreshold;

		/// <summary>
		/// Минимальная длина контуров.
		/// </summary>
        private int minLength;

		/// <summary>
		/// Максимальная длина контуров.
		/// </summary>
		private int maxLength;

		/// <summary>
		/// Вызывает событие, сообщающее о том, что произошли изменения.
		/// </summary>
        private void OnChanged() => this.Changed?.Invoke(this, EventArgs.Empty);
    }
}
