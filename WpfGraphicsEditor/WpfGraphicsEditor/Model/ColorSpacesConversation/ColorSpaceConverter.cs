﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using WpfGraphicsEditor.Model.ColorSpacesConversation;

namespace WpfGraphicsEditor.Model
{
	/// <summary>
	/// Класс для конвертации цветовых пространств.
	/// </summary>
    public static class ColorSpaceConverter
    {
		/// <summary>
		/// Преобразует RGB каналы изображения в YCbCr каналы.
		/// </summary>
		/// <param name="red">Красный канал.</param>
		/// <param name="green">Зеленый канал.</param>
		/// <param name="blue">Синий канал.</param>
		/// <returns>Кортеж YCbCr каналов.</returns>
		/// <exception cref="Exception">Выбрасывается если red, green или blue являются null.</exception>
		public static (byte[] y, byte[] cb, byte[] cr) convertRGBToYCbCr(byte[] red, byte[] green, byte[] blue)
        {
            if (red == null || green == null || blue == null)
            {
                throw new Exception("Массивы red, green, blue должны быть заполнены");
            }
            int length = red.Length;
            byte[] y = new byte[length];
            byte[] cb = new byte[length];
            byte[] cr = new byte[length];

            for (int i = 0; i < length; i++)
            {
                byte R = red[i];
                byte G = green[i];
                byte B = blue[i];

                y[i] = getFromRGB(R, G, B, YfromRGB);
                cb[i] = getFromRGB(R, G, B, CbfromRGB);
                cr[i] = getFromRGB(R, G, B, CrfromRGB);

            }
            return (y, cb, cr);

        }

		/// <summary>
		/// Преобразует YCbCr каналы изображения в RGB каналы.
		/// </summary>
		/// <param name="y">Компонента яркости.</param>
		/// <param name="cb">Синяя цветоразностная компонента.</param>
		/// <param name="cr">Красная цветоразностная компонента.</param>
		/// <returns>Кортеж каналов RGB.</returns>
		/// <exception cref="Exception">Выбрасывается если y, cb, cr являются null.</exception>
		/// <exception cref="Exception">Выбрасывается если y, cb, cr имеют разный размер.</exception>
		public static (byte[] red, byte[] green, byte[] blue) convertYCbCrToRGB(byte[] y, byte[] cb, byte[] cr)
        {
            if (y == null || cb == null || cr == null)
            {
                throw new Exception("Массивы y, cb, cr должны быть заполнены.");
            }
            if (y.Length != cb.Length || y.Length != cr.Length)
            {
                throw new Exception("Массивы y, cb, cr должны быть одинакового размера.");
            }

            int length = y.Length;
            byte[] red = new byte[length];
            byte[] green = new byte[length];
            byte[] blue = new byte[length];
            
            for (int i = 0; i < length; i++)
            {
                byte Y = y[i];
                byte Cb = cb[i];
                byte Cr = cr[i];
                red[i] = getFromYCbCr(Y, Cb, Cr, RfromYCbCr);
                green[i] = getFromYCbCr(Y, Cb, Cr, GfromYCbCr);
                blue[i] = getFromYCbCr(Y, Cb, Cr, BfromYCbCr);
            }
            return (red, green, blue);
        }

		/// <summary>
		/// Преобразует значение YCbCr компонент в значение субпикселя.
		/// </summary>
		/// <param name="У">Компонента яркости.</param>
		/// <param name="Сb">Синяя цветоразностная компонента.</param>
		/// <param name="Сr">Красная цветоразностная компонента.</param>
		/// <param name="vector">Вектор конвертации.</param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static byte getFromYCbCr(byte Y, byte Cb, byte Cr, CofficientsYCbCr vector)
        {
            double subpixelValue =
                vector.Const +
                vector.CoefficientY * Y +
                vector.CoefficientCb * Cb +
                vector.CoefficientCr * Cr;
            return ByteConverter.ToByte(subpixelValue);
        }

		/// <summary>
		/// Преобразует значение RGB компонент в значение субпикселя.
		/// </summary>
		/// <param name="R">Красная составляющая.</param>
		/// <param name="G">Зеленая составляющая.</param>
		/// <param name="B">Синяя составляющая.</param>
		/// <param name="vector"></param>
		/// <returns>Значение субпикселя.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static byte getFromRGB(byte R, byte G, byte B, CofficientsRGB vector)
        {
            double subpixelValue =
                vector.Const +
                vector.CoefficientR * R +
                vector.CoefficientG * G +
                vector.CoefficientB * B;
            return ByteConverter.ToByte(subpixelValue);
        }

		/// <summary>
		/// Вектор преобразования значений RGB в компоненту яркости.
		/// </summary>
        public static CofficientsRGB YfromRGB { get; set; } = new CofficientsRGB(YCbCrModelJPEG.YfromRGB);

		/// <summary>
		/// Вектор преобразования значений RGB в синюю цветоразностную компоненту.
		/// </summary>
		public static CofficientsRGB CbfromRGB { get; set; } = new CofficientsRGB(YCbCrModelJPEG.CbfromRGB);

		/// <summary>
		/// Вектор преобразования значений RGB в красную цветоразностную компоненту.
		/// </summary>
		public static CofficientsRGB CrfromRGB { get; set; } = new CofficientsRGB(YCbCrModelJPEG.CrfromRGB);


		/// <summary>
		/// Вектор преобразования значений YCbCr в красную составляющую.
		/// </summary>
		public static CofficientsYCbCr RfromYCbCr { get; set; } = new CofficientsYCbCr(YCbCrModelJPEG.RfromYCbCr);

		/// <summary>
		/// Вектор преобразования значений YCbCr в зеленую составляющую.
		/// </summary>
		public static CofficientsYCbCr GfromYCbCr { get; set; } = new CofficientsYCbCr(YCbCrModelJPEG.GfromYCbCr);

		/// <summary>
		/// Вектор преобразования значений YCbCr в синюю составляющую.
		/// </summary>
		public static CofficientsYCbCr BfromYCbCr { get; set; } = new CofficientsYCbCr(YCbCrModelJPEG.BfromYCbCr);
    }
}
