﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfGraphicsEditor.Model.ColorSpacesConversation
{
	/// <summary>
	/// Класс, хранящий коэфициенты преобразования YCbCr в RGB и обратно на основе модели JPEG.
	/// </summary>
    public static class YCbCrModelJPEG
    {
		/// <summary>
		/// Коэфициенты для получения компоненты яркости из RGB составляющих.
		/// </summary>
        public static CofficientsRGB YfromRGB = new CofficientsRGB(
            Const: 0.5,
            CoefficientR: 0.299,
            CoefficientG: 0.587,
            CoefficientB: 0.114
        );

		/// <summary>
		/// Коэфициенты для получения синей цветоразностной компоненты из RGB составляющих.
		/// </summary>
		public static CofficientsRGB CbfromRGB = new CofficientsRGB(
            Const: 128.5,
            CoefficientR: -0.168736,
            CoefficientG: - 0.331264,
            CoefficientB: 0.5
        );

		/// <summary>
		/// Коэфициенты для получения красной цветоразностной компоненты из RGB составляющих.
		/// </summary>
		public static CofficientsRGB CrfromRGB = new CofficientsRGB(
            Const: 128.5,
            CoefficientR: 0.5,
            CoefficientG: - 0.418688,
            CoefficientB: - 0.081312
        );

		/// <summary>
		/// Коэфициенты для получения красной составляющей из компонент YCbCr.
		/// </summary>
		public static CofficientsYCbCr RfromYCbCr = new CofficientsYCbCr(
            Const: 0.5 - 128 * 1.402,
            CoefficientY: 1,
            CoefficientCb: 0,
            CoefficientCr: 1.402
        );

		/// <summary>
		/// Коэфициенты для получения зеленой составляющей из компонент YCbCr.
		/// </summary>
		public static CofficientsYCbCr GfromYCbCr = new CofficientsYCbCr(
            Const: 0.5 + 128 * (0.34414 + 0.71414),
            CoefficientY: 1,
            CoefficientCb: -0.34414,
            CoefficientCr: -0.71414
        );

		/// <summary>
		/// Коэфициенты для получения синей составляющей из компонент YCbCr.
		/// </summary>
		public static CofficientsYCbCr BfromYCbCr = new CofficientsYCbCr(
            Const: 0.5 - 128 * 1.772,
            CoefficientY: 1,
            CoefficientCb: 1.772,
            CoefficientCr: 0
        );
    }                                                  
}                                                      
