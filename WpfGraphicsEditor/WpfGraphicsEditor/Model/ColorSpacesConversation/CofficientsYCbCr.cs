﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfGraphicsEditor.Model.ColorSpacesConversation
{
	/// <summary>
	/// Класс, используемый для хранения коэфициентов преобразования из цветового пространства YCbCr.
	/// </summary>
	public class CofficientsYCbCr
    {
		/// <summary>
		/// Конструктор.
		/// </summary>
		/// <param name="Const">Постоянная составляющая.</param>
		/// <param name="CoefficientY">Коэфициент для яркости.</param>
		/// <param name="CoefficientCb">Коэфициент для синей цветоразностной компоненты.</param>
		/// <param name="CoefficientCr">Коэфициент для красной цветоразностной компоненты.</param>
		public CofficientsYCbCr(double Const, double CoefficientY, double CoefficientCb, double CoefficientCr)
        {
            this.Const = Const;
            this.CoefficientY = CoefficientY;
            this.CoefficientCb = CoefficientCb;
            this.CoefficientCr = CoefficientCr;
        }

		/// <summary>
		/// Конструктор нового объекта на основе другого объекта.
		/// </summary>
		/// <param name="original">Исходный объект.</param>
		public CofficientsYCbCr(CofficientsYCbCr original)
        {
            this.Const = original.Const;
            this.CoefficientY = original.CoefficientY;
            this.CoefficientCb = original.CoefficientCb;
            this.CoefficientCr = original.CoefficientCr;
        }

		/// <summary>
		/// Постоянная составляющая.
		/// </summary>
		public double Const { get; set; }
		/// <summary>
		/// Коэфициент для яркости.
		/// </summary>
		public double CoefficientY { get; set; }

		/// <summary>
		/// <param name="CoefficientCb">Коэфициент для синей цветоразностной компоненты.</param>
		/// </summary>
		public double CoefficientCb { get; set; }

		/// <summary>
		/// Коэфициент для красной цветоразностной компоненты.
		/// </summary>
		public double CoefficientCr { get; set; }
    }
}
