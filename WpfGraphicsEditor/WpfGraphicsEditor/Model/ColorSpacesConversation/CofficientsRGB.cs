﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfGraphicsEditor.Model.ColorSpacesConversation
{
	/// <summary>
	/// Класс, используемый для хранения коэфициентов преобразования из цветового пространства RGB.
	/// </summary>
	public class CofficientsRGB
    {
		/// <summary>
		/// Конструктор.
		/// </summary>
		/// <param name="Const">Постоянная составляющая.</param>
		/// <param name="CoefficientR">Коэфициент для красной составляющей.</param>
		/// <param name="CoefficientG">Коэфициент для зеленой составляющей.</param>
		/// <param name="CoefficientB">Коэфициент для синей составляющей.</param>
		public CofficientsRGB(double Const, double CoefficientR, double CoefficientG, double CoefficientB)
        {
            this.Const = Const;
            this.CoefficientR = CoefficientR;
            this.CoefficientG = CoefficientG;
            this.CoefficientB = CoefficientB;
        }

		/// <summary>
		/// Создает новый объект на основе другого объекта.
		/// </summary>
		/// <param name="original">Исходный объект.</param>
        public CofficientsRGB(CofficientsRGB original)
        {
            this.Const = original.Const;
            this.CoefficientR = original.CoefficientR;
            this.CoefficientG = original.CoefficientG;
            this.CoefficientB = original.CoefficientB;
        }

		/// <summary>
		/// Постоянная составляющая.
		/// </summary>
		public double Const { get; set; }

		/// <summary>
		/// Коэфициент для красной составляющей.
		/// </summary>
		public double CoefficientR { get; set; }

		/// <summary>
		/// Коэфициент для зеленой составляющей.
		/// </summary>
		public double CoefficientG { get; set; }

		/// <summary>
		/// Коэфициент для синей составляющей.
		/// </summary>
		public double CoefficientB { get; set; }
    }
}
