﻿using OpenCvSharp;
using OpenCvSharp.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace WpfGraphicsEditor.Model
{
	/// <summary>
	/// Класс модели для поиска контуров.
	/// </summary>
	class ContoursSearch
    {
		/// <summary>
		/// Конструктор.
		/// </summary>
		/// <param name="bitmap">Битовая карта изображения, на котором небходимо найти контуры.</param>
        public ContoursSearch(BitmapSource bitmap)
        {
            if (bitmap.Format != PixelFormats.Bgr24)
            {
                bitmap = ImageHelper.ConvertToBgr24(bitmap);
            }

            original = bitmap.ToMat();
            brightness = original.CvtColor(ColorConversionCodes.BGR2GRAY);

            Mat[] channels = Cv2.Split(original);
            blueChannel = channels[0];
            greenChannel = channels[1];
            redChannel = channels[2];

            brightnessContours = new List<ContourRange>();
            redContours = new List<ContourRange>();
            greenContours = new List<ContourRange>();
            blueContours = new List<ContourRange>();

            BrightnessContoured = RedContoured = GreenContoured = BlueContoured = bitmap;
        }

		/// <summary>
		/// Происходит при обновлении изображения с контурами, найденными для составляющей яркости.
		/// </summary>
        public event EventHandler BrightnessChanged;

		/// <summary>
		/// Происходит при обновлении изображения с контурами, найденными для красного канала.
		/// </summary>
		public event EventHandler RedChanged;

		/// <summary>
		/// Происходит при обновлении изображения с контурами, найденными для зеленого канала.
		/// </summary>
		public event EventHandler GreenChanged;

		/// <summary>
		/// Происходит при обновлении изображения с контурами, найденными для синего канала.
		/// </summary>
		public event EventHandler BlueChanged;

		/// <summary>
		/// Изображение с контурами, найденными для составляющей яркости.
		/// </summary>
		public BitmapSource BrightnessContoured
        {
            get => brightnessContoured;
            set
            {
                brightnessContoured = value;
                OnBrightnessChanged();
            }
        }

		/// <summary>
		/// Изображение с контурами, найденными для красного канала.
		/// </summary>
		public BitmapSource RedContoured
        {
            get => redContoured;
            set
            {
                redContoured = value;
                OnRedChanged();
            }
		}

		/// <summary>
		/// Изображение с контурами, найденными для зеленого канала.
		/// </summary>

		public BitmapSource GreenContoured
        {
            get => greenContoured;
            set
            {
                greenContoured = value;
                OnGreenChanged();
            }
		}

		/// <summary>
		/// Изображение с контурами, найденными для синего канала.
		/// </summary>

		public BitmapSource BlueContoured
        {
            get => blueContoured;
            set
            {
                blueContoured = value;
                OnBlueChanged();
            }
		}

		/// <summary>
		/// Изображение с контурами, найденными для 
		/// </summary>

		public bool IsBackgroundDisplayed
        {
            get => isBackgroundDisplayed;
            set
            {
                isBackgroundDisplayed = value;
                RefrechBrightness();
                RefreshRed();
                RefreshGreen();
                RefreshBlue();
            }
        }

		/// <summary>
		/// Метод для добавления нового диапазона контуров.
		/// </summary>
		/// <param name="color">Цвет отображения контуров.</param>
		/// <param name="thickness">Толщина.</param>
		/// <param name="lowThreshold">Нижний порог яркости изображения, начиная с которого ищутся контуры.</param>
		/// <param name="highThreshold">Нижний порог яркости изображения, начиная с которого ищутся контуры.</param>
		/// <param name="minLength">Минимальная длина искомых контуров.</param>
		/// <param name="maxLength">Максимальная длина искомых контуров</param>
		/// <returns>Список добавленных диапазонов канал(по одному на каждый RGB канал + составляющую яркости).</returns>
		public List<ContourRange> AddRange(Color color, int thickness, int lowThreshold, int highThreshold, int minLength, int maxLength)
        {
            ContourRange brightnessRange = new ContourRange(brightness, color, thickness, lowThreshold, highThreshold, minLength, maxLength);
            brightnessContours.Add(brightnessRange);
            brightnessRange.Changed += BrightnessRange_Changed;

            ContourRange redRange = new ContourRange(redChannel, color, thickness, lowThreshold, highThreshold, minLength, maxLength);
            redContours.Add(redRange);
            redRange.Changed += RedRange_Changed;

            ContourRange greenRange = new ContourRange(greenChannel, color, thickness, lowThreshold, highThreshold, minLength, maxLength);
            greenContours.Add(greenRange);
            greenRange.Changed += GreenRange_Changed;

            ContourRange blueRange = new ContourRange(blueChannel, color, thickness, lowThreshold, highThreshold, minLength, maxLength);
            blueContours.Add(blueRange);
            blueRange.Changed += BlueRange_Changed;

            return new List<ContourRange> { brightnessRange, redRange, greenRange, blueRange };
        }

		/// <summary>
		/// Исходное изображени.
		/// </summary>
        private Mat original;

		/// <summary>
		/// Составляющая яркости.
		/// </summary>
        private Mat brightness;

		/// <summary>
		/// Красный канал.
		/// </summary>
        private Mat redChannel;

		/// <summary>
		/// Зеленый канал.
		/// </summary>
        private Mat greenChannel;

		/// <summary>
		/// Синий канал.
		/// </summary>
        private Mat blueChannel;

		/// <summary>
		/// Контуры, найденные для составляющей яркости.
		/// </summary>
        private List<ContourRange> brightnessContours;

		/// <summary>
		/// Контуры, найденные для красного канала.
		/// </summary>
		private List<ContourRange> redContours;

		/// <summary>
		/// Контуры, найденные для зеленого канала.
		/// </summary>
		private List<ContourRange> greenContours;

		/// <summary>
		/// Контуры, найденные для красного канала.
		/// </summary>
		private List<ContourRange> blueContours;

		/// <summary>
		/// Изображение с контурами, найденными для составляющей яркости.
		/// </summary>
		private BitmapSource brightnessContoured;

		/// <summary>
		/// Изображение с контурами, найденными для красного канала.
		/// </summary>
		private BitmapSource redContoured;

		/// <summary>
		/// Изображение с контурами, найденными для зеленого канала.
		/// </summary>
		private BitmapSource greenContoured;

		/// <summary>
		/// Изображение с контурами, найденными для синего канала.
		/// </summary>
		private BitmapSource blueContoured;

		/// <summary>
		/// Отображать ли фон.
		/// </summary>
        private bool isBackgroundDisplayed = true;

		/// <summary>
		/// Вызывает событие, сообщающее об изменении изображения с найденными контурами для составляющей яркости.
		/// </summary>
        private void OnBrightnessChanged() => BrightnessChanged?.Invoke(this, EventArgs.Empty);

		/// <summary>
		/// Вызывает событие, сообщающее об изменении изображения с найденными контурами для красного канала.
		/// </summary>
		private void OnRedChanged() => RedChanged?.Invoke(this, EventArgs.Empty);

		/// <summary>
		/// Вызывает событие, сообщающее об изменении изображения с найденными контурами для зеленого канала.
		/// </summary>
		private void OnGreenChanged() => GreenChanged?.Invoke(this, EventArgs.Empty);

		/// <summary>
		/// Вызывает событие, сообщающее об изменении изображения с найденными контурами для синего канала.
		/// </summary>
		private void OnBlueChanged() => BlueChanged?.Invoke(this, EventArgs.Empty);

		/// <summary>
		/// Обработчик изменения диапазона контуров для составляющей яркости.
		/// </summary>
        private void BrightnessRange_Changed(object sender, EventArgs e) => RefrechBrightness();

		/// <summary>
		/// Обработчик изменения диапазона контуров для красного канала.
		/// </summary>
		private void RedRange_Changed(object sender, EventArgs e) => RefreshRed();

		/// <summary>
		/// Обработчик изменения диапазона контуров для зеленого канала.
		/// </summary>
		private void GreenRange_Changed(object sender, EventArgs e) => RefreshGreen();

		/// <summary>
		/// Обработчик изменения диапазона контуров для синего канала.
		/// </summary>
		private void BlueRange_Changed(object sender, EventArgs e) => RefreshBlue();

		/// <summary>
		/// Метод, обновляющий контуры, найденные на канале яркости изображения.
		/// </summary>
        private void RefrechBrightness()
        {
            BrightnessContoured = DrawContoursRanges(brightnessContours);
        }

		/// <summary>
		/// Метод, обновляющий контуры, найденные на красном канале изображения.
		/// </summary>
		private void RefreshRed()
        {
            RedContoured = DrawContoursRanges(redContours);
        }

		/// <summary>
		/// Метод, обновляющий контуры, найденные на зеленом канале изображения.
		/// </summary>
		private void RefreshGreen()
        {
            GreenContoured = DrawContoursRanges(greenContours);
        }

		/// <summary>
		/// Метод, обновляющий контуры, найденные на синем канале изображения.
		/// </summary>
		private void RefreshBlue()
        {
            BlueContoured = DrawContoursRanges(blueContours);
        }

		/// <summary>
		/// Отображает контур, используя исходное изображение как фон.
		/// </summary>
		/// <param name="rangesList">Список, содержащий диапазоны контуров</param>
		/// <returns></returns>
        private WriteableBitmap DrawContoursRanges(List<ContourRange> rangesList)
        {
            Mat background = new Mat();
            try
            {
                if (isBackgroundDisplayed)
                {
                    background = original.Clone();
                }
                else
                {
                    background = new Mat(original.Size(), original.Type());
                    background.SetTo(new Scalar(255, 255, 255));
                }

                foreach (ContourRange contour in rangesList)
                {
                    if (contour.IsActive)
                    {
                        contour.DrawContour(background);
                    }
                }
                return background.ToWriteableBitmap();
            }
            catch
            {
                background.Dispose();
                return null;
            }
        }

    }
}
