﻿using OpenCvSharp;
using OpenCvSharp.Extensions;
using System;
using System.Linq;
using System.Windows.Media.Imaging;

namespace WpfGraphicsEditor.Model
{
	/// <summary>
	/// Класс для нахождения сигнатуры средних значений яркости / градиента изображения по вертикали.
	/// </summary>
    class Signature
    {
		/// <summary>
		/// Конструктор.
		/// </summary>
		/// <param name="bitmap">Изображение, для которого ищутся средние значения.</param>
		/// <param name="normalizationMinimum">Минимум яркости для нормализованного изображения сигнатуры.</param>
		/// <param name="normalizationMaximum">Максимум яркости для нормализованного изображения сигнатуры.</param>
		/// <param name="isGradientModeEnabled">Если true (по умолчанию), то ищутся градиенты, если false - то средние значения яркости.</param>
		/// <param name="isRelativityNormalizingEnabled">Если true , то ищутся градиенты, если false (по умолчанию) - то средние значения яркости.</param>
		public Signature(
			byte[,] brightness,
            byte normalizationMinimum = byte.MinValue, 
            byte normalizationMaximum = byte.MaxValue, 
            bool isGradientModeEnabled = true, 
            bool isRelativityNormalizingEnabled = false
        )
        {
            this.normalizationMinimum = normalizationMinimum;
            this.normalizationMaximum = normalizationMaximum;
            this.isGradientModeEnabled = isGradientModeEnabled;
            this.isRelativityNormalizingEnabled = isRelativityNormalizingEnabled;
			this.brightness = brightness;

			UpdateSignature();
        }

		/// <summary>
		/// Событие изменения сигнатуры.
		/// </summary>
        public event EventHandler Changed;

		/// <summary>
		/// Рассчитывать ли градиенты.
		/// </summary>
        public bool IsGradientsModeEnabled
        {
            get => isGradientModeEnabled;
            set
            {
                isGradientModeEnabled = value;
                UpdateSignature();
            }
        }

		/// <summary>
		/// Если <see langword="true"/>, то нормализация учитывает действительные минимальные и максимальные значения сигнатуры. 
		/// и уже их приводит к заданному диапазону нормализации. Если <see langword="false"/>, то для приведения используются теоретически 
		/// возможные максимальные и минимальные значения.
		/// </summary>
		public bool IsRelativityNormalizingEnabled
        {
            get => isRelativityNormalizingEnabled;
            set
            {
                isRelativityNormalizingEnabled = value;
                UpdateSignature();
            }
        }

		/// <summary>
		/// Минимум нормализованной сигнатуры.
		/// </summary>
		public byte NormalizationMinimum
        {
            get => normalizationMinimum;
            set
            {
                normalizationMinimum = value;
                UpdateSignature();
            }
        }

		/// <summary>
		/// Максимум нормализованной сигнатуры.
		/// </summary>
        public byte NormalizationMaximum
        {
            get => normalizationMaximum;
            set
            {
                normalizationMaximum = value;
                UpdateSignature();
            }
        }

		/// <summary>
		/// Полоса сигнатуры.
		/// </summary>
        public byte[] SignatureRow
        {
            get => verticalSignature;
            private set
            {
                verticalSignature = value;
                OnChanged();
            }
		}

		/// <summary>
		/// Обновляет сигнатуру.
		/// </summary>
		public void UpdateSignature()
		{
			SignatureRow = getSignature(brightness);
		}

		private byte[] getSignature(byte[,] array)
		{
			return isGradientModeEnabled ?
				CalculateAverageGradients(array) :
				CalculateAverageBrightness(array);
		}

		/// <summary>
		/// Минимальное значение градиента.
		/// </summary>
		private const int MIN_GRADIENT_VALUE = 0;

		/// <summary>
		/// Максимальное значение градиента.
		/// </summary>
		private const int MAX_GRADIENT_VALUE = 255;

		/// <summary>
		/// Минимальное значение яркости.
		/// </summary>
		private const int MIN_BRIGHTNESS_VALUE = 0;

		/// <summary>
		/// Максимальное значение яркости.
		/// </summary>
		private const int MAX_BRIGHTNESS_VALUE = 255;

		/// <summary>
		/// Вертикальная сигнатура.
		/// </summary>
		private byte[] verticalSignature;

		/// <summary>
		/// Если <see langword="true"/>, то нормализация учитывает действительные минимальные и максимальные значения сигнатуры. 
		/// и уже их приводит к заданному диапазону нормализации. Если <see langword="false"/>, то для приведения используются теоретически.
		/// возможные максимальные и минимальные значения.
		/// </summary>
		private bool isRelativityNormalizingEnabled;
        private bool isGradientModeEnabled;

		/// <summary>
		/// Минимум нормализованной сигнатуры.
		/// </summary>
		private byte normalizationMinimum;

		/// <summary>
		/// Максимум нормализованной сигнатуры.
		/// </summary>
		private byte normalizationMaximum;

		/// <summary>
		/// Матрица яркости изображения.
		/// </summary>
		private readonly byte[,] brightness;
		/// <summary>
		/// Вызывает событие изменения сигнатуры.
		/// </summary>
		protected virtual void OnChanged() => this.Changed?.Invoke(this, EventArgs.Empty);

		/// <summary>
		/// Рассчитывает средние градиенты изображения по вертикали.
		/// </summary>
		/// <returns>Массив средних градиентов.</returns>
        private byte[] CalculateAverageGradients(byte[,] matrix)
        {
			int height = matrix.GetLength(0);
			int width = matrix.GetLength(1);
			
            float[,] gradients = calculateGradients(matrix);
			float[] averageGradients = new float[width];
            for (int w = 0; w < width; w++)
            {
                float gradient = 0;
				for (int h = 0; h < height; h++)
                {
                    gradient += Math.Abs(gradients[h, w]);
                }

                gradient /= height;

                averageGradients[w] = gradient;
            }

            return IsRelativityNormalizingEnabled ? normalizeRelatively(averageGradients) : 
                normalize(averageGradients, MIN_GRADIENT_VALUE, MAX_GRADIENT_VALUE, normalizationMinimum, normalizationMaximum);
        }

		/// <summary>
		/// Рассчитывает среднюю яркость изображения по вертикали.
		/// </summary>
		/// <returns>Массив, средней яркости.</returns>
		private byte[] CalculateAverageBrightness(byte[,] matrix)
		{
			int height = matrix.GetLength(0);
			int width = matrix.GetLength(1);

			float[] average = new float[width];
            for (int w = 0; w < width; w++)
            {
                float total = 0;
                for (int h = 0; h < height; h++)
                {
                    total += matrix[h, w];
                }

                total /= height;

                average[w] = total;
            }

            return IsRelativityNormalizingEnabled ? normalizeRelatively(average) : 
                normalize(average, MIN_BRIGHTNESS_VALUE, MAX_BRIGHTNESS_VALUE, normalizationMinimum, normalizationMaximum);
        }

        /// <summary>
		/// Рассчитывает горизонтальные градиенты для изображения.
		/// </summary>
		/// <param name="matrix">Матрица изображения.</param>
		/// <returns>Матрица градиентов.</returns>
        private float[,] calculateGradients(byte[,] matrix)
        {
            int heigth = matrix.GetLength(0);
            int width = matrix.GetLength(1);
            var gradients = new float[heigth, width];

            float cornerCoefficient = (float)(1 / Math.Sqrt(2));
            float mainNormalizeCoefficient = 1 / (1 + 2 * cornerCoefficient);
            float additionalNormalizeCoefficient = 1 / (1 + cornerCoefficient);

            // Вычисляем градиенты для первой строки.
            for (int w = 0; w < width - 1; w++)
            {
                byte currentPixelBrightness = matrix[0, w];
                byte rightPixelBrightness = matrix[0, w + 1];
                byte bottomRightCornerBrightness = matrix[1, w + 1];

                gradients[1, w] = additionalNormalizeCoefficient * (
                    currentPixelBrightness - rightPixelBrightness +
                    (currentPixelBrightness - bottomRightCornerBrightness) * cornerCoefficient);
            }

            // Вычисляем градиенты для последней строки.
            int currentHeight = heigth - 1;
            for (int w = 0; w < width - 1; w++)
            {
                byte currentPixelBrightness = matrix[currentHeight, w];
                byte rightPixelBrightness = matrix[currentHeight, w + 1];
                byte topRightCornerBrightness = matrix[currentHeight - 1, w + 1];

                gradients[1, w] = additionalNormalizeCoefficient * (
                    currentPixelBrightness - rightPixelBrightness +
                    (currentPixelBrightness - topRightCornerBrightness) * cornerCoefficient);
            }

            // Вычисляем градиенты для последнего столбца.
            int currentWidth = width - 1;
            for (int h = 0; h < heigth - 1; h++)
            {
                gradients[h, currentWidth] = 0;
            }

            // Вычисляем градиенты для оставшейся части матрицы.
            for (int h = 1; h < heigth - 1; h++)
            {
                for (int w = 0; w < width - 1; w++)
                {
                    byte currentPixelBrightness = matrix[h, w];
                    byte rightPixelBrightness = matrix[h, w + 1];
                    byte topRightCornerBrightness = matrix[h - 1, w + 1];
                    byte bottomRightCornerBrightness = matrix[h + 1, w + 1];
                    gradients[h, w] =
                        currentPixelBrightness - rightPixelBrightness +
                        (currentPixelBrightness - topRightCornerBrightness +
                        currentPixelBrightness - bottomRightCornerBrightness) * cornerCoefficient;
                }
            }
            return gradients;
		}

		/// <summary>
		/// Осуществляет относительную нормализацию, учитывая минимальные и максимальные значения в массиве.
		/// </summary>
		/// <param name="original">Массив, который нужно нормализовать.</param>
		/// <returns>Нормализованный массив.</returns>
        private byte[] normalizeRelatively(float[] original)
        {
            float minDataValue = original.Min();
            float maxDataValue = original.Max();
            return normalize(original, minDataValue, maxDataValue, normalizationMinimum, normalizationMaximum);
        }

		/// <summary>
		/// Нормализует массив.
		/// </summary>
		/// <param name="original">Исходный массив.</param>
		/// <param name="minInputValue">Значение в исходном массиве, которое должно стать минимальным в новом диапазоне значений.</param>
		/// <param name="maxInputValue">Значение в исходном массиве, которое должно стать максимальным в новом диапазоне значений.</param>
		/// <param name="minOutputValue">Минимальное значение в нормализованном массиве.</param>
		/// <param name="maxOutputValue">Максимальное значение в нормализованном массиве.</param>
		/// <returns>Нормализованный массив.</returns>
		private static byte[] normalize(float[] original, float minInputValue, float maxInputValue, byte minOutputValue, byte maxOutputValue)
        {
            int length = original.Length;
            float k = (maxOutputValue - minOutputValue) / (maxInputValue - minInputValue);

            byte[] normalized = new byte[length];
            for (int i = 0; i < length; i++)
            {
                float value = minOutputValue + k * (original[i] - minInputValue);
                normalized[i] = (byte)(
                    value > 255 ? 255
                        : value < 0 ? 0
                            : value);
            }

            return normalized;
        }
    }
}