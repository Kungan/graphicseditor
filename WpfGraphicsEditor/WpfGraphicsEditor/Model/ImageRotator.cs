﻿using OpenCvSharp;
using OpenCvSharp.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace WpfGraphicsEditor.Model
{
	/// <summary>
	/// Класс для автоматического выравнивания изображения.
	/// </summary>
    static class ImageRotator
    {
		/// <summary>
		/// Выровнять изображение.
		/// </summary>
		/// <param name="bitmap">Исходное изображение.</param>
		/// <returns>Выровненое изображени.</returns>
        public static BitmapSource AutoRotate(BitmapSource bitmap)
        {
            Mat original = bitmap.ToMat();
            double angle = GetAutoRotationAngle(original);
            Mat rotated = Rotate(original, angle);
            return rotated.ToBitmapSource();
        }

		/// <summary>
		/// Поворачивает изобржение.
		/// </summary>
		/// <param name="image">Исходная матрица изображения.</param>
		/// <param name="rotationAngle">Угол поворота.</param>
		/// <returns>Повернутая матрица изображения.</returns>
        private static Mat Rotate(Mat image, double rotationAngle)
        {
            Point2f center = new Point2f(image.Cols / 2f, image.Rows / 2f);
            Mat rotationMatrix = Cv2.GetRotationMatrix2D(
                center: center,
                angle: rotationAngle * 180 / Math.PI,
                scale: 1
            );
            Mat rotatedImage = new Mat();
            Cv2.WarpAffine(
                src: image,
                dst: rotatedImage,
                m: rotationMatrix,
                dsize: image.Size(),
                flags: InterpolationFlags.Cubic,
                borderMode: BorderTypes.Transparent
            );
            return rotatedImage;
        }

		/// <summary>
		/// Вычисляет угол поворота для выравнивания.
		/// </summary>
		/// <param name="image">Матрица изображения.</param>
		/// <returns>Угол поворота.</returns>
        private static double GetAutoRotationAngle(Mat image)
        {
            IEnumerable<double> angles = calcAngles(image);

            double phi = 0.0;
            double e = 0.01;
            foreach (double a in angles)
            {
                foreach (double b in angles)
                {
                    double difference = Math.Abs(a - b);
                    if (Math.PI / 2 - e < difference && difference < Math.PI / 2 + e)
                    {
                        phi = Math.Abs(a) < Math.Abs(b) ? a : b;
                        break;
                    }
                }
            }
            return phi;
        }

		/// <summary>
		/// Вычисляет углы линий, найденных на изображении.
		/// </summary>
		/// <param name="image">Изображение.</param>
		/// <param name="drawline">Отображать ли линии на изображении.</param>
		/// <returns>Углы.</returns>
        private static IEnumerable<double> calcAngles(Mat image, bool drawline = true)
        {
            int height = image.Rows;
            int width = image.Cols;
            Mat brightness = image.CvtColor(ColorConversionCodes.BGR2GRAY);
            Mat edges = new Mat();

            Cv2.Canny(
                src: brightness,
                edges: edges,
                threshold1: 50,
                threshold2: 150,
                apertureSize: 3
            );

            LineSegmentPoint[] lines = Cv2.HoughLinesP(
                image: edges,
                rho: 1,
                theta: Math.PI / 180,
                threshold: 70,
                minLineLength: 100,
                maxLineGap: 7
            );

            List<double> angles = new List<double>();
            foreach (LineSegmentPoint line in lines)
            {
                Point point1 = line.P1;
                Point point2 = line.P2;

                double dx = point2.X - point1.X;
                double dy = point2.Y - point1.Y;
                double angle = (dx == 0) ? Math.PI / 2 : Math.Atan(dy / dx);

                angles.Add(angle);
            }
            return angles;
        }
    }
}
