﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace WpfGraphicsEditor
{
	/// <summary>
	/// Класс, расширяющий класс Shape (базового класса для всех фигур).
	/// </summary>
	public static class ShapeExtention
	{

		/// <summary>
		/// Устанавливает параметры прямоугольника такими, чтобы он соответствовал границам фигуры.
		/// </summary>
		/// <param name="shape">Фигура.</param>
		/// <param name="rect">Прямоугольни.</param>
		public static void SetBounds (this Shape shape, Rectangle rect)
		{
			if (shape is Line)
			{
				var line = shape as Line;

				rect.Width = Math.Abs(line.X2 - line.X1);
				rect.Height = Math.Abs(line.Y2 - line.Y1);
				rect.Margin = new Thickness(Math.Min(line.X1, line.X2), Math.Min(line.Y1, line.Y2), 0, 0);
			}
			else if (shape is Rectangle || shape is Ellipse)
			{
				rect.Margin = shape.Margin;
				rect.Width = shape.Width;
				rect.Height = shape.Height;
			}
			else if(shape is Polyline || shape is Polygon)
			{
				PointCollection points;
				if (shape is Polyline)
				{
					points = (shape as Polyline).Points;
				}

				else
				{
					points = (shape as Polygon).Points;
				}

				double top = double.MaxValue;
				double bottom = double.MinValue;
				double left = double.MaxValue;
				double right = double.MinValue;

				foreach(Point point in points)
				{
					if (point.Y < top)
					{
						top = point.Y;
					}
					if (point.Y > bottom)
					{
						bottom = point.Y;
					}
					if (point.X < left)
					{
						left = point.X;
					}
					if (point.X > right)
					{
						right = point.X;
					}
				}

				rect.Margin = new Thickness(left, top, 0, 0);
				rect.Width = right - left;
				rect.Height = bottom - top;
			}
		}
	}
}
