﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfGraphicsEditor.ViewModel
{
	/// <summary>
	/// Класс модели представления изменяемого значения.
	/// </summary>
	/// <typeparam name="T">Тип изменямого значения.</typeparam>
    public class UpDownSliderVM<T> : BaseViewModel
    {
		/// <summary>
		/// Конструктор без параметров для корректного предпросмотра XAML кода.
		/// </summary>
        public UpDownSliderVM()
        {
        }

		/// <summary>
		/// Конструктор.
		/// </summary>
		/// <param name="title">Название значения.</param>
		/// <param name="defaultValue">Значение по умолчанию.</param>
		/// <param name="increment">Инкремент.</param>
		/// <param name="minimum">Минимум.</param>
		/// <param name="maximum">Максимум.</param>
        public UpDownSliderVM(string title, T defaultValue, T increment, T minimum, T maximum)
        {
            value = defaultValue;
            Title = title;
            Increment = increment;
            Minimum = minimum;
            Maximum = maximum;
        }

		/// <summary>
		/// Название значения.
		/// </summary>
		public string Title { get; }

		/// <summary>
		/// Инкремент.
		/// </summary>
        public T Increment
        {
            get => increment;
            set
            {
                increment = value;
                OnPropertyChanged();
            }
        }

		/// <summary>
		/// Минимум.
		/// </summary>
        public T Minimum { get; }

		/// <summary>
		/// Максимум.
		/// </summary>
        public T Maximum { get; }

		/// <summary>
		/// Значение.
		/// </summary>
        private T value;

		/// <summary>
		/// Инкремент.
		/// </summary>
        private T increment;

		/// <summary>
		/// Значение.
		/// </summary>
		public T Value
        {
            get => value;
            set
            {
                this.value = value;
                OnPropertyChanged();
            }
        }
    }
}
