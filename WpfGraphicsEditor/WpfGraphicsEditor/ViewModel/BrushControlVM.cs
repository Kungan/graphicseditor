﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Data;

namespace WpfGraphicsEditor
{
	/// <summary>
	/// Модель представления элемента управления кистью.
	/// </summary>
	[NotifyPropertyChanged]
	public class BrushControlVM: DependencyObject
	{
		/// <summary>
		/// Конструктор.
		/// </summary>
		/// <param name="selectedColor">Цвет кисти.</param>
		public BrushControlVM(Color selectedColor)
		{
			gradientStops = new GradientStopCollection();

			Point startPoint = new Point(0, 0.5);
			Point endPoint = new Point(1, 0.5);

			linearBrush = new LinearGradientBrush(gradientStops, startPoint, endPoint);
			radialBrush = new RadialGradientBrush(gradientStops);
			solidBrush = new SolidColorBrush();

			BrushType = BrushType.Solid;

			SelectedColor = selectedColor;
			Opacity = 1;
		}
		
		/// <summary>
		/// Добавляет цвет в градиент.
		/// </summary>
		public GradientStop AddColor()
		{
			GradientStop gradientStop = new GradientStop(SelectedColor, 0);
			gradientStops.Add(gradientStop);
			Notice = "";
			return gradientStop;
		}

		/// <summary>
		/// Стандартное сообщение, выводимое, если в градиент не добавлены цвета.
		/// </summary>
		private const string noticeGradientStopsEmpty = "Добавьте цвета";

		/// <summary>
		/// Сообение на месте отображения градиента.
		/// </summary>
		public string Notice { get; set; }
		
		/// <summary>
		/// Сплошная кисть.
		/// </summary>
		private SolidColorBrush solidBrush;

		/// <summary>
		/// Кисть с линейным градиентом.
		/// </summary>
		private LinearGradientBrush linearBrush;

		/// <summary>
		/// Кисть с радиальным градиентом.
		/// </summary>
		private RadialGradientBrush radialBrush;

		/// <summary>
		/// Коллекция градиентных цветов.
		/// </summary>
		private GradientStopCollection gradientStops;

		/// <summary>
		/// Свойство зависимости для текущей кисти.
		/// </summary>
		public static readonly DependencyProperty CurrentBrushProperty =
			DependencyProperty.Register(nameof(CurrentBrush), typeof(Brush), typeof(BrushControlVM),
				new FrameworkPropertyMetadata(Brushes.Black, new PropertyChangedCallback(OnCurrentBrushChanged)));

		/// <summary>
		/// Текущая кисть.
		/// </summary>
		public Brush CurrentBrush
		{
			get
			{
				object brush = GetValue(CurrentBrushProperty);
				return (Brush)brush;
			}
			set
			{
				SetValue(CurrentBrushProperty, value);
			}
		}

		/// <summary>
		/// Событие изменения кисти.
		/// </summary>
		public event EventHandler BrushChangedEvent;

		/// <summary>
		/// Вызывает событие изменения кисти.
		/// </summary>
		private void RaseBrushChanged()
		{
			BrushChangedEvent?.Invoke(this, EventArgs.Empty);
		}

		/// <summary>
		/// Обработчик изменения кисти.
		/// </summary>
		private static void OnCurrentBrushChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
		{
			BrushControlVM model = sender as BrushControlVM;
			model.RaseBrushChanged();
		}

		/// <summary>
		/// Тип кисти.
		/// </summary>
		BrushType brushType;

		/// <summary>
		/// Тип кисти.
		/// </summary>
		public BrushType BrushType
		{
			get { return brushType; }

			set
			{
				brushType = value;
				switch (value)
				{
					case BrushType.Solid:
						CurrentBrush = solidBrush;
						Notice = "";
						GradientStopsVisibility = Visibility.Collapsed;
						break;
					case BrushType.LinearGradient:
						CurrentBrush = linearBrush;
						if (gradientStops.Count == 0)
						{
							Notice = noticeGradientStopsEmpty;
						}
						GradientStopsVisibility = Visibility.Visible;
						break;
					case BrushType.RadialGradient:
						CurrentBrush = radialBrush;
						if (gradientStops.Count == 0)
						{
							Notice = noticeGradientStopsEmpty;
						}
						GradientStopsVisibility = Visibility.Visible;
						break;
				}
			}
		}

		/// <summary>
		/// Цвет сплошной кисти.
		/// </summary>
		private Color selectedColor;

		/// <summary>
		/// Выбранный цвет.
		/// </summary>
		public Color SelectedColor
		{
			get
			{
				return selectedColor;
			}

			set
			{
				selectedColor = value;
				solidBrush.Color = value;
			}
		}

		/// <summary>
		/// Прозрачность.
		/// </summary>
		public double opacity;

		/// <summary>
		/// Прозрачность.
		/// </summary>
		public double Opacity
		{
			get { return opacity; }

			set
			{
				opacity = value;
				solidBrush.Opacity = value;
				linearBrush.Opacity = value;
				radialBrush.Opacity = value;
			}
		}
		
		/// <summary>
		/// Видимость градиентов.
		/// </summary>
		public Visibility GradientStopsVisibility { get; set; }

		/// <summary>
		/// Координата начальной точки линейного градиента по оси X.
		/// </summary>
		public double LinearBrush_StartPoint_X
		{
			get
			{
				return linearBrush.StartPoint.X;
			}

			set
			{
				linearBrush.StartPoint = new Point(value, linearBrush.StartPoint.Y);
			}
		}

		/// <summary>
		/// Координата начальной точки линейного градиента по оси Y.
		/// </summary>
		public double LinearBrush_StartPoint_Y
		{
			get
			{
				return linearBrush.StartPoint.Y;
			}

			set
			{
				linearBrush.StartPoint = new Point(linearBrush.StartPoint.X, value);
			}
		}

		/// <summary>
		/// Координата конечной точки линейного градиента по оси X.
		/// </summary>
		public double LinearBrush_EndPoint_X
		{
			get
			{
				return linearBrush.EndPoint.X;
			}

			set
			{
				linearBrush.EndPoint = new Point(value, linearBrush.EndPoint.Y);
			}
		}


		/// <summary>
		/// Координата конечной точки линейного градиента по оси Y.
		/// </summary>
		public double LinearBrush_EndPoint_Y
		{
			get
			{
				return linearBrush.EndPoint.Y;
			}

			set
			{
				linearBrush.EndPoint = new Point(linearBrush.EndPoint.X, value);
			}
		}


		/// <summary>
		/// Координата центра радиального градиента по оси X.
		/// </summary>
		public double RadialBrush_GradientOrigin_X
		{
			get
			{
				return radialBrush.GradientOrigin.X;
			}

			set
			{
				radialBrush.GradientOrigin = new System.Windows.Point(value, radialBrush.GradientOrigin.Y);
			}
		}

		/// <summary>
		/// Координата центра радиального градиента по оси Y.
		/// </summary>
		public double RadialBrush_GradientOrigin_Y
		{
			get
			{
				return radialBrush.GradientOrigin.Y;
			}

			set
			{
				radialBrush.GradientOrigin = new Point(radialBrush.GradientOrigin.X, value);
			}
		}

		/// <summary>
		/// Координата центра кисти по оси X.
		/// </summary>
		public double RadialBrush_Center_X
		{
			get
			{
				return radialBrush.Center.X;
			}

			set
			{
				radialBrush.Center = new Point(value, radialBrush.Center.Y);
			}
		}

		/// <summary>
		/// Координата центра кисти по оси Y.
		/// </summary>
		public double RadialBrush_Center_Y
		{
			get
			{
				return radialBrush.Center.Y;
			}

			set
			{
				radialBrush.Center = new Point(radialBrush.Center.X, value);
			}
		}

		/// <summary>
		/// Высота радиальной кисти.
		/// </summary>
		public double RadialBrush_RadiusX
		{
			get
			{
				return radialBrush.RadiusX;
			}

			set
			{
				radialBrush.RadiusX = value;
			}
		}


		/// <summary>
		/// Ширина радиальной кисти.
		/// </summary>
		public double RadialBrush_RadiusY
		{
			get
			{
				return radialBrush.RadiusY;
			}

			set
			{
				radialBrush.RadiusY = value;
			}
		}

	}
}
