﻿using System.Windows.Input;

namespace WpfGraphicsEditor.ViewModel
{
	/// <summary>
	/// Класс для хранения общих команд.
	/// </summary>
	public class ToolsCommands
	{
		/// <summary>
		/// Статический конструктор.
		/// </summary>
		static ToolsCommands()
		{
			InvertColors = new RoutedCommand("InvertColors", typeof(MainWindow));
			ApplyChanges = new RoutedCommand("ApplyChanges", typeof(MainWindow));
			CancelChanges = new RoutedCommand("CancelChanges", typeof(MainWindow));
			ShowGradients = new RoutedCommand("ShowGradients", typeof(MainWindow));
			Clarity = new RoutedCommand("Сlarity", typeof(MainWindow));
			Borders = new RoutedCommand("Borders", typeof(MainWindow));
			Blur = new RoutedCommand("Blur", typeof(MainWindow));
			UnSharpening = new RoutedCommand("UnSharpening", typeof(MainWindow));
			GaussianBluring = new RoutedCommand("GaussianBluring", typeof(MainWindow));
			ResizeBitmap = new RoutedCommand("ResizeBitmap", typeof(MainWindow));
			Clip = new RoutedCommand("Clip", typeof(MainWindow));
			Ruler = new RoutedCommand("Ruler", typeof(MainWindow));
			Grid = new RoutedCommand("Grid", typeof(MainWindow));
			Rasterisation = new RoutedCommand("Rasterizing", typeof(MainWindow));
			AddColorToGradient = new RoutedCommand("AddColorToGradient", typeof(BrushControl)); 
            Signature = new RoutedCommand("Signature", typeof(BrushControl));
            ContourSearch = new RoutedCommand("ContourSearch", typeof(BrushControl));
            AutoRotate = new RoutedCommand("AutoRotate", typeof(MainWindow));
            ColorSpaceParameters = new RoutedCommand("ColorSpaceParameters", typeof(MainWindow));
        }

		/// <summary>
		/// Инвертировать цвета.
		/// </summary>
		public static RoutedCommand InvertColors { get; set; }

		/// <summary>
		/// Применить изменения.
		/// </summary>
		public static RoutedCommand ApplyChanges { get; set; }

		/// <summary>
		/// Отменить изменения.
		/// </summary>
		public static RoutedCommand CancelChanges { get; set; }

		/// <summary>
		/// Показать градиенты.
		/// </summary>
		public static RoutedCommand ShowGradients { get; set; }

		/// <summary>
		/// Увеличить четкость.
		/// </summary>
		public static RoutedCommand Clarity { get; set; }

		/// <summary>
		/// Выделение границ.
		/// </summary>
		public static RoutedCommand Borders { get; set; }

		/// <summary>
		/// Размытие.
		/// </summary>
		public static RoutedCommand Blur { get; set; }

		/// <summary>
		/// Границы + резкость.
		/// </summary>
		public static RoutedCommand UnSharpening { get; set; }

		/// <summary>
		/// Гауссово размытие.
		/// </summary>
		public static RoutedCommand GaussianBluring { get; set; }

		/// <summary>
		/// Изменить размеры изображения.
		/// </summary>
		public static RoutedCommand ResizeBitmap { get; set; }

		/// <summary>
		/// Обрезать.
		/// </summary>
		public static RoutedCommand Clip { get; set; }

		/// <summary>
		/// Отобразить линейку.
		/// </summary>
		public static RoutedCommand Ruler { get; set; }

		/// <summary>
		/// Отобразить сетку.
		/// </summary>
		public static RoutedCommand Grid { get; set; }

		/// <summary>
		/// Добавить цвет в градиент кисти.
		/// </summary>
		public static RoutedCommand AddColorToGradient { get; set; }

		/// <summary>
		/// Растеризовать векторное изображение.
		/// </summary>
        public static RoutedCommand Rasterisation { get; set; }

		/// <summary>
		/// Отобразить сигнатуру изображения.
		/// </summary>
        public static RoutedCommand Signature { get; private set; }

		/// <summary>
		/// Поиск контуров.
		/// </summary>
        public static RoutedCommand ContourSearch { get; }

		/// <summary>
		/// Выровнять.
		/// </summary>
        public static RoutedCommand AutoRotate { get; }

		/// <summary>
		/// Настройки конвертации цветовых пространств.
		/// </summary>
        public static RoutedCommand ColorSpaceParameters { get; }
    }
}
