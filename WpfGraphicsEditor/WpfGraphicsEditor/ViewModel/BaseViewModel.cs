﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace WpfGraphicsEditor.ViewModel
{
	/// <summary>
	/// Базовый класс для моделей представления.
	/// </summary>
    public abstract class BaseViewModel : INotifyPropertyChanged
    {
		/// <summary>
		/// Событие изменения свойства.
		/// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Вызывает событие при изменении свойства.
		/// </summary>
		/// <param name="propertyName">Название свойства.</param>
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
