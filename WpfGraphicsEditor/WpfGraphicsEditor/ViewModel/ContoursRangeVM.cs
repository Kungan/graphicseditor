﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.ComponentModel;
using WpfGraphicsEditor.Model;

namespace WpfGraphicsEditor.ViewModel
{
	/// <summary>
	/// Класс модели представления для диапазона контуров.
	/// Позволяет управлять параметрами сразу набороа диапазонов контуров - по диапазону на каждый канал.
	/// </summary>
    class ContoursRangeVM
    {
		/// <summary>
		/// Конструктор без параметров для корректной привязки и предпросмотра в XAML конструкторе.
		/// </summary>
        public ContoursRangeVM() { }

		/// <summary>
		/// Конструктор.
		/// </summary>
		/// <param name="contourRangesList">Список, содержащий диапазоны контуров.</param>
        public ContoursRangeVM(List<ContourRange> contourRangesList)
        {
            this.contourRangesList = contourRangesList;

            IsActive = contourRangesList.First().IsActive;
            ContourColor = contourRangesList.First().Color;

            LowThresholdVM = new UpDownSliderVM<double>(
                title: "Нижний порог",
                defaultValue: contourRangesList.First().LowThreshold,
                increment: 1,
                minimum: 0,
                maximum: 255
            );
            LowThresholdVM.PropertyChanged += LowThresholdVM_PropertyChanged;

            HighThresholdVM = new UpDownSliderVM<double>(
                title: "Верхний порог",
                defaultValue: contourRangesList.First().HighThreshold,
                increment: 1,
                minimum: 0,
                maximum: 255
            );
            HighThresholdVM.PropertyChanged += HighThresholdVM_PropertyChanged;

            ThicknessVM = new UpDownSliderVM<int>(
                title: "Толщина контура",
                defaultValue: contourRangesList.First().Thickness,
                increment: 1,
                minimum: 1,
                maximum: 10
            );
            ThicknessVM.PropertyChanged += ThicknessVM_PropertyChanged;

            MinLengthVM = new UpDownSliderVM<int>(
                title: "Минимальная длина контура",
                defaultValue: contourRangesList.First().MinLength,
                increment: 1,
                minimum: 0,
                maximum: 10000
            );
            MinLengthVM.PropertyChanged += MinLengthVM_PropertyChanged;

            MaxLengthVM = new UpDownSliderVM<int>(
                title: "Максимальная длина контура",
                defaultValue: contourRangesList.First().MaxLength,
                increment: 1,
                minimum: 0,
                maximum: 10000
            );
            MaxLengthVM.PropertyChanged += MaxLengthVM_PropertyChanged;

            AviableStepValues = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            Step = AviableStepValues[0];
        }
        
		/// <summary>
		/// Модель представления для управления нижним порогом яркости, начиная с которого ищутся контуры.
		/// </summary>
        public UpDownSliderVM<double> LowThresholdVM { get; }

		/// <summary>
		/// Модель представления для управления верхним порогом яркости, начиная с которого ищутся контуры.
		/// </summary>
		public UpDownSliderVM<double> HighThresholdVM { get; }

		/// <summary>
		/// Модель представления для управления минимальной длиной контуров.
		/// </summary>
		public UpDownSliderVM<int> MinLengthVM { get; }

		/// <summary>
		/// Модель представления для управления максимальной длиной контуров.
		/// </summary>
		public UpDownSliderVM<int> MaxLengthVM { get; }
		
		/// <summary>
		/// Модель представления для управления толщиной отображаемых контуров.
		/// </summary>
		public UpDownSliderVM<int> ThicknessVM { get; }

		/// <summary>
		/// Массив для выбора доступных шагов изменения яркости.
		/// </summary>
        public int[] AviableStepValues { get; }

		/// <summary>
		/// Активен ли диапазон контуров.
		/// </summary>
        private bool isActive;

		/// <summary>
		/// Активен ли диапазон контуров.
		/// </summary>
        public bool IsActive
        {
            get => isActive;
            set
            {
                isActive = value;
                contourRangesList.ForEach(range => range.IsActive = isActive);
            }
        }

		/// <summary>
		/// Шаг изменения параметров, управлеяющий минимальной и максимальной яркостью области изображения, на которой ищутся контуры.
		/// </summary>
        public int Step
        {
            get => step;
            set
            {
                step = value;
                LowThresholdVM.Increment = HighThresholdVM.Increment = step;
            }
        }

		/// <summary>
		/// Цвет контуров.
		/// </summary>
        public Color ContourColor
        {
            get => contourColor;
            set
            {
                contourColor = value;
                contourRangesList.ForEach(range => range.Color = contourColor);
            }
        }

		/// <summary>
		/// Шаг изменения параметров, управлеяющий минимальной и максимальной яркостью области изображения, на которой ищутся контуры.
		/// </summary>
		private int step;

		/// <summary>
		/// Цвет контуров.
		/// </summary>
		private Color contourColor;

		/// <summary>
		/// Список дипапазонов контуров.
		/// </summary>
		private List<ContourRange> contourRangesList;

		#region Обработчики событий, возникающих при изменении пользователем параметров диапазонов контуров.

		/// <summary>
		/// Обновляет максимальную длину.
		/// </summary>
		private void MaxLengthVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(UpDownSliderVM<int>.Value))
            {
                contourRangesList.ForEach(range => range.MaxLength = MaxLengthVM.Value);
            }
        }

		/// <summary>
		/// Обновляет минимальную длину.
		/// </summary>
		private void MinLengthVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(UpDownSliderVM<int>.Value))
            {
                contourRangesList.ForEach(range => range.MinLength = MinLengthVM.Value);
            }
        }


		/// <summary>
		/// Обновляет толщину.
		/// </summary>
		private void ThicknessVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(UpDownSliderVM<int>.Value))
            {
                contourRangesList.ForEach(range => range.Thickness = ThicknessVM.Value);
            }
        }

		/// <summary>
		/// Обновляет максимальный порог яркости.
		/// </summary>
		private void HighThresholdVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(UpDownSliderVM<double>.Value))
            {
                contourRangesList.ForEach(range => range.HighThreshold = HighThresholdVM.Value);
            }
        }

		/// <summary>
		/// Обновляет минимальный порог яркости.
		/// </summary>
		private void LowThresholdVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(UpDownSliderVM<double>.Value))
            {
                contourRangesList.ForEach(range => range.LowThreshold = LowThresholdVM.Value);
            }
        }
		#endregion
	}
}
