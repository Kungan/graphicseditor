﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using WpfGraphicsEditor.Model;

namespace WpfGraphicsEditor.ViewModel
{
	/// <summary>
	/// Класс модели представления для окна поиска контуров.
	/// </summary>
    class ContoursSearchVM : BaseViewModel
	{
		/// <summary>
		/// Конструктор без параметров для корректной привязки и предпросмотра в XAML конструкторе.
		/// </summary>
		public ContoursSearchVM() {}

		/// <summary>
		/// Конструктор.
		/// </summary>
		/// <param name="contourSearchModel">Объект модели поиска контуров.</param>
        public ContoursSearchVM (ContoursSearch contourSearchModel)
        {
            this.contourSearchModel = contourSearchModel;
            this.contourSearchModel.BrightnessChanged += (o, e) => YBitmap = contourSearchModel.BrightnessContoured;
            this.contourSearchModel.RedChanged += (o, e) => RBitmap = contourSearchModel.RedContoured;
            this.contourSearchModel.GreenChanged += (o, e) => GBitmap = contourSearchModel.GreenContoured;
            this.contourSearchModel.BlueChanged += (o, e) => BBitmap = contourSearchModel.BlueContoured;

            AddRangeCommand = new RelayCommand(_ => AddRange());

            ContourRanges = new ObservableCollection<ContoursRangeVM>();

            AddRange();
        }

		/// <summary>
		/// Коллекция моделей представления для диапазона контуров.
		/// </summary>
        public ObservableCollection<ContoursRangeVM> ContourRanges { get; }

		/// <summary>
		/// Команда добавления диапазона контуров.
		/// </summary>
        public RelayCommand AddRangeCommand { get; }

		/// <summary>
		/// Отображать ли фон на изображении с контурами.
		/// </summary>
        public bool IsBackgroundDisplayed
        {
            get => contourSearchModel.IsBackgroundDisplayed;
            set
            {
                contourSearchModel.IsBackgroundDisplayed = value;
                OnPropertyChanged();
            }
        }

		/// <summary>
		/// Изображение, на котором отображаются контуры для составляющей яркости.
		/// </summary>
        public BitmapSource YBitmap
        {
            get => yBitmap;
            private set
            {
                yBitmap = value;
                OnPropertyChanged();
            }
        }

		/// <summary>
		/// Изображение, на котором отображаются контуры для красного канала.
		/// </summary>
		public BitmapSource RBitmap
        {
            get => rBitmap;
            private set
            {
                rBitmap = value;
                OnPropertyChanged();
            }
        }

		/// <summary>
		/// Изображение, на котором отображаются контуры для зеленого канала.
		/// </summary>
		public BitmapSource GBitmap
        {
            get => gBitmap;
            private set
            {
                gBitmap = value;
                OnPropertyChanged();
            }
        }


		/// <summary>
		/// Изображение, на котором отображаются контуры для синего канала.
		/// </summary>
		public BitmapSource BBitmap
        {
            get => bBitmap;
            private set
            {
                bBitmap = value;
                OnPropertyChanged();
            }
        }

		/// <summary>
		/// Объект модели поиска контуров.
		/// </summary>
        private ContoursSearch contourSearchModel;

		/// <summary>
		/// Изображение, на котором отображаются контуры для составляющей яркости.
		/// </summary>
		private BitmapSource yBitmap;

		/// <summary>
		/// Изображение, на котором отображаются контуры для красного канала.
		/// </summary>
		private BitmapSource rBitmap;

		/// <summary>
		/// Изображение, на котором отображаются контуры для зеленого канала.
		/// </summary>
		private BitmapSource gBitmap;

		/// <summary>
		/// Изображение, на котором отображаются контуры для синего канала.
		/// </summary>
		private BitmapSource bBitmap;
        
		/// <summary>
		/// Метод, добавляющий новый диапазон контуров.
		/// </summary>
        private void AddRange()
        {
            List<ContourRange> rangesList = contourSearchModel.AddRange(
                color: Colors.Red,
                thickness: 1,
                lowThreshold: 0,
                highThreshold: 255,
                minLength: 0,
                maxLength: 10000
            );

            ContourRanges.Add(new ContoursRangeVM(rangesList));
        }
    }
}
