﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfGraphicsEditor.ViewModel
{
	public class RangeVM: BaseViewModel
	{
		/// <summary>
		/// Конструктор без параметров для корректного предпросмотра XAML кода.
		/// </summary>
		public RangeVM()
		{
		}

		/// <summary>
		/// Конструктор. Нижнее и верхнее значение по умолчанию устанавливается равным минимуму и максимуму значений.
		/// </summary>
		/// <param name="title">Название значения</param>
		/// <param name="increment">Инкремент.</param>
		/// <param name="minimum">Минимум.</param>
		/// <param name="maximum">Максимум.</param>
		public RangeVM(string title, double increment, double minimum, double maximum)
		{
			Title = title;
			Increment = increment;
			Minimum = minimum;
			Maximum = maximum;
			lowerValue = minimum;
			higherValue = maximum;
		}

		/// <summary>
		/// Конструктор.
		/// </summary>
		/// <param name="title">Название значения</param>
		/// <param name="increment">Инкремент.</param>
		/// <param name="minimum">Минимум.</param>
		/// <param name="maximum">Максимум.</param>
		/// <param name="lowerValue">Нижнее значение по умолчанию.</param>
		/// <param name="higherValue">Верхнее значение по умолчанию.</param>
		public RangeVM(string title, double increment, double minimum, double maximum, double lowerValue, double higherValue)
		{
			this.Title = title;
			this.Increment = increment;
			this.Minimum = minimum;
			this.Maximum = maximum;
			this.lowerValue = lowerValue;
			this.higherValue = higherValue;
		}

		/// <summary>
		/// Название значения.
		/// </summary>
		public string Title { get; }

		/// <summary>
		/// Инкремент.
		/// </summary>
		public double Increment
		{
			get => increment;
			set
			{
				increment = value;
				OnPropertyChanged();
			}
		}

		/// <summary>
		/// Минимум.
		/// </summary>
		public double Minimum { get; }

		/// <summary>
		/// Максимум.
		/// </summary>
		public double Maximum { get; }

		/// <summary>
		/// Нижнее значение.
		/// </summary>
		private double lowerValue;

		/// <summary>
		/// Верхнее значение.
		/// </summary>
		private double higherValue;

		/// <summary>
		/// Инкремент.
		/// </summary>
		private double increment;

		/// <summary>
		/// Значение.
		/// </summary>
		public double LowerValue
		{
			get => lowerValue;
			set
			{
				this.lowerValue = value;
				OnPropertyChanged();
			}
		}
		/// <summary>
		/// Значение.
		/// </summary>
		public double HigherValue
		{
			get => higherValue;
			set
			{
				this.higherValue = value;
				OnPropertyChanged();
			}
		}
	}
}
