﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfGraphicsEditor.Model;
using WpfGraphicsEditor.Model.ColorSpacesConversation;

namespace WpfGraphicsEditor.ViewModel
{

    public class ColorSpaceParametersVM : BaseViewModel
    {
		/// <summary>
		/// Конструктор.
		/// </summary>
        public ColorSpaceParametersVM()
        {
            ApplyCommand = new RelayCommand(_ => Apply());
            SetDefaultCommand = new RelayCommand(_ => SetDefault());
            QuitCommand = new RelayCommand(_ => Quit());
        }

		/// <summary>
		/// Комманда "Применить".
		/// </summary>
        public RelayCommand ApplyCommand { get; }

		/// <summary>
		/// Команда "Сбросить на значения по умолчнанию".
		/// </summary>
        public RelayCommand SetDefaultCommand { get; }

		/// <summary>
		/// Команда "Выйти".
		/// </summary>
        public RelayCommand QuitCommand { get; }


		/// <summary>
		/// Вектор для получения компоненты яркости из каналов RGB.
		/// </summary>
		private CofficientsRGB yfromRGB = ColorSpaceConverter.YfromRGB;

		/// <summary>
		/// Вектор для получения компоненты яркости из каналов RGB.
		/// </summary>
		public CofficientsRGB YfromRGB
        {
            get => yfromRGB;
            set
            {
                yfromRGB = value;
                OnPropertyChanged();
            }
		}

		/// <summary>
		/// Вектор для получения синей цветоразностной компоненты из каналов RGB.
		/// </summary>
		private CofficientsRGB cbfromRGB = ColorSpaceConverter.CbfromRGB;

		/// <summary>
		/// Вектор для получения синей цветоразностной компоненты из каналов RGB.
		/// </summary>
		public CofficientsRGB CbfromRGB
        {
            get => cbfromRGB;
            set
            {
                cbfromRGB = value;
                OnPropertyChanged();
            }
        }


		/// <summary>
		/// Вектор для получения красной цветоразностной компоненты из каналов RGB.
		/// </summary>
		private CofficientsRGB crfromRGB = ColorSpaceConverter.CrfromRGB;

		/// <summary>
		/// Вектор для получения красной цветоразностной компоненты из каналов RGB.
		/// </summary>
		public CofficientsRGB CrfromRGB
        {
            get => crfromRGB;
            set
            {
                crfromRGB = value;
                OnPropertyChanged();
            }
        }


		/// <summary>
		/// Вектор для получения красного канала из компонент YCbCr.
		/// </summary>
		private CofficientsYCbCr rfromYCbCr = ColorSpaceConverter.RfromYCbCr;

		/// <summary>
		/// Вектор для получения красного канала из компонент YCbCr.
		/// </summary>
		public CofficientsYCbCr RfromYCbCr
        {
            get => rfromYCbCr;
            set
            {
                rfromYCbCr = value;
                OnPropertyChanged();
            }
		}


		/// <summary>
		/// Вектор для получения зеленого канала из компонент YCbCr.
		/// </summary>
		private CofficientsYCbCr gfromYCbCr = ColorSpaceConverter.GfromYCbCr;

		/// <summary>
		/// Вектор для получения зеленого канала из компонент YCbCr.
		/// </summary>
		public CofficientsYCbCr GfromYCbCr
        {
            get => gfromYCbCr;
            set
            {
                gfromYCbCr = value;
                OnPropertyChanged();
            }
		}


		/// <summary>
		/// Вектор для получения синего канала из компонент YCbCr.
		/// </summary>
		private CofficientsYCbCr bfromYCbCr = ColorSpaceConverter.BfromYCbCr;

		/// <summary>
		/// Вектор для получения синего канала из компонент YCbCr.
		/// </summary>
		public CofficientsYCbCr BfromYCbCr
        {
            get => bfromYCbCr;
            set
            {
                bfromYCbCr = value;
                OnPropertyChanged();
            }
        }

		/// <summary>
		/// Применяет изменения.
		/// </summary>
        private void Apply()
        {
            ColorSpaceConverter.YfromRGB = YfromRGB;
            ColorSpaceConverter.CbfromRGB = CbfromRGB;
            ColorSpaceConverter.CrfromRGB = CrfromRGB;

            ColorSpaceConverter.RfromYCbCr = RfromYCbCr;
            ColorSpaceConverter.GfromYCbCr = GfromYCbCr;
            ColorSpaceConverter.BfromYCbCr = BfromYCbCr;

            OnPropertyChanged();
        }

		/// <summary>
		/// Сбрасывает параметры на значения по умолчанию.
		/// </summary>
        private void SetDefault()
        {
            YfromRGB = new CofficientsRGB(YCbCrModelJPEG.YfromRGB);
            CbfromRGB = new CofficientsRGB(YCbCrModelJPEG.CbfromRGB);
            CrfromRGB = new CofficientsRGB(YCbCrModelJPEG.CrfromRGB);

            RfromYCbCr = new CofficientsYCbCr(YCbCrModelJPEG.RfromYCbCr);
            GfromYCbCr = new CofficientsYCbCr(YCbCrModelJPEG.GfromYCbCr);
            BfromYCbCr = new CofficientsYCbCr(YCbCrModelJPEG.BfromYCbCr);
        }

		/// <summary>
		/// Выйти. 
		/// </summary>
        private void Quit()
        {
            OnPropertyChanged();
        }

    }
}
