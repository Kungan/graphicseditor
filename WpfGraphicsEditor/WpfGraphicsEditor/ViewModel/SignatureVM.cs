﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using WpfGraphicsEditor.Model;

namespace WpfGraphicsEditor.ViewModel
{
	/// <summary>
	/// Класс модели представления нахождения сигнатуры изображения.
	/// </summary>
    class SignatureVM : BaseViewModel
	{
		/// <summary>
		/// Конструктор без параметров для корректной привязки и предпросмотра в XAML конструкторе.
		/// </summary>
		public SignatureVM() {}

		/// <summary>
		/// Конструктор.
		/// </summary>
		/// <param name="brightness">Матрица изображения, для которого производится поиск сигнатуры.</param>
		public SignatureVM(byte[,] brightness)
        {
            signatureModel = new Signature(brightness);
            signatureModel.Changed += SignatureModel_Changed;


            NormalizationMinimumVM = new UpDownSliderVM<byte>(
                title: "Минимум",
                defaultValue: signatureModel.NormalizationMinimum,
                increment: 1,
                minimum: 0,
                maximum: 255
            );
            NormalizationMinimumVM.PropertyChanged += NormalizationMinimumVM_PropertyChanged;

            NormalizationMaximumVM = new UpDownSliderVM<byte>(
                title: "Максимум",
                defaultValue: signatureModel.NormalizationMaximum,
                increment: 1,
                minimum: 0,
                maximum: 255
            );
            NormalizationMaximumVM.PropertyChanged += NormalizationMaximumVM_PropertyChanged;

			NormalizationRange = new RangeVM(
				title: "Диапазон нормализации", 
				increment: 1, 
				minimum: byte.MinValue, 
				maximum: byte.MaxValue,
				lowerValue: signatureModel.NormalizationMinimum,
				higherValue: signatureModel.NormalizationMaximum
			);
			NormalizationRange.PropertyChanged += NormalizationRange_PropertyChanged;

			UpdateSignature();
        }

		/// <summary>
		/// Включен ли режим расчета градиентов.
		/// </summary>
		public bool IsGradientsModeEnabled
        {
            get => signatureModel.IsGradientsModeEnabled;
            set
            {
                signatureModel.IsGradientsModeEnabled = value;
            }
        }

		/// <summary>
		/// Включена ли относительная нормализация (основанная на действительном диапазоне значений в сигнатуре), 
		/// либо абсолютная (основанная на теоретически возможном диапазоне значений сигнатуры).
		/// </summary>
        public bool IsRelativityNormalizingEnabled
        {
            get => signatureModel.IsRelativityNormalizingEnabled;
            set
            {
                signatureModel.IsRelativityNormalizingEnabled = value;
            }
        }

		/// <summary>
		/// Модель представления для настройки минимума нормализации.
		/// </summary>
        public UpDownSliderVM<byte> NormalizationMinimumVM { get; }

		/// <summary>
		/// Модель представления для настройки максимума нормализации.
		/// </summary>
		public UpDownSliderVM<byte> NormalizationMaximumVM { get; }

		public RangeVM NormalizationRange { get; }

		/// <summary>
		/// Модель сигнатуры.
		/// </summary>
		private Signature signatureModel;

		/// <summary>
		/// Обработчик изменения сигнатуры.
		/// </summary>
        private void SignatureModel_Changed(object sender, EventArgs e)
        {
            UpdateSignature();
        }

		/// <summary>
		/// Обновляет кисть на основе сигнатуры.
		/// </summary>
        private void UpdateSignature()
        {
			byte[] signatureRow = signatureModel.SignatureRow;
			Signature = ImageHelper.composeBitmapByRGB(signatureRow, signatureRow, signatureRow, 1, signatureRow.Length);
		}

		BitmapSource signature;

		/// <summary>
		/// Кисть горизонтальной сигнатуры.
		/// </summary>
		public BitmapSource Signature
		{
			get => signature;
			set
			{
				signature = value;
				OnPropertyChanged();
			}
		}

		/// <summary>
		/// Обработчик изменения пользователем минимума нормализации.
		/// </summary>
		private void NormalizationMinimumVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(UpDownSliderVM<int>.Value))
            {
                signatureModel.NormalizationMinimum = NormalizationMinimumVM.Value;
            }
        }

		/// <summary>
		/// Обработчик изменения пользователем максимума нормализации.
		/// </summary>
		private void NormalizationMaximumVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(UpDownSliderVM<int>.Value))
            {
                signatureModel.NormalizationMaximum = NormalizationMaximumVM.Value;
            }
        }

		private void NormalizationRange_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			if (e.PropertyName == nameof(RangeVM.LowerValue)) {
				signatureModel.NormalizationMinimum = Model.ByteConverter.ToByte(NormalizationRange.LowerValue);
			}
			else if (e.PropertyName == nameof(RangeVM.HigherValue)) {
				signatureModel.NormalizationMaximum = Model.ByteConverter.ToByte(NormalizationRange.HigherValue);
			}

		}
    }
}
