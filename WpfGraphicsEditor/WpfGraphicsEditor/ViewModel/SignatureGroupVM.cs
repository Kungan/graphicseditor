﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using WpfGraphicsEditor.Model;

namespace WpfGraphicsEditor.ViewModel
{
	/// <summary>
	/// Класс представляющий собой модель представления группы сигнатуры (горизонтальной и вертикальной).
	/// </summary>
	class SignatureGroupVM: BaseViewModel
	{
		/// <summary>
		/// Конструктор.
		/// </summary>
		/// <param name="bitmap">Битовая карта изображения, сигнатуру которого необходимо найти.</param>
		public SignatureGroupVM(BitmapSource bitmap)
		{
			Bitmap = bitmap;

			(byte[] red, byte[] green, byte[] blue) = ImageHelper.decomposeSourceToRGB(bitmap);

			(byte[] Y, _, _) = ColorSpaceConverter.convertRGBToYCbCr(red, green, blue);

			byte[,] brightness = createMatrix(Y, bitmap.PixelWidth);
			VerticalSignatureVM = new SignatureVM(brightness);

			byte[,] rotatedBrightness = rotate(brightness);
			HorizontalSignatureVM = new SignatureVM(rotatedBrightness);
		}

		/// <summary>
		/// Изображение, для которого производится поиск сигнатуры.
		/// </summary>
		public BitmapSource Bitmap { get; }

		/// <summary>
		/// Модель представления вертикальной сигнатуры.
		/// </summary>
		public SignatureVM VerticalSignatureVM { get; }

		/// <summary>
		/// Модель представления горизонтальной сигнатуры.
		/// </summary>
		public SignatureVM HorizontalSignatureVM { get; }

		/// <summary>
		/// Создает двумерный массив из одномерного.
		/// </summary>
		/// <param name="array">Одномерный массив.</param>
		/// <param name="width">Требуемая ширина двумерного массива.</param>
		/// <returns>Двумерный массив.</returns>
		/// <exception cref="ArgumentException">Выбрасывается если длина массива не делится нацело на ширину изображения.</exception>
		private byte[,] createMatrix(byte[] array, int width)
		{
			int length = array.Length;
			if (length % width != 0) {
				throw new ArgumentException("Длина массива не делится нацело на ширину изображения");
			}
			int height = length / width;
			var matrix = new byte[height, width];
			for (int h = 0; h < height; h++) {
				for (int w = 0; w < width; w++) {
					matrix[h, w] = array[h * width + w];
				}
			}
			return matrix;
		}

		/// <summary>
		/// Поворачивает двумерный массив на 90 градусов против часовой стрелки.
		/// </summary>
		/// <param name="array">Исходный массив.</param>
		/// <returns>Повернутый массив.</returns>
		private byte[,] rotate(byte[,] array)
		{
			int height = array.GetLength(0);
			int width = array.GetLength(1);
			byte[,] rotatedArray = new byte[width, height];
			byte[] row = new byte[width];
			for (int h = 0; h < height; h++) {
				for (int w = 0; w < width; w++) {
					{
						rotatedArray[width - w - 1, h] = array[h, w];
					}
				}
			}
			return rotatedArray;
		}
	}
}
