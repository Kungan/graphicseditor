﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace WpfGraphicsEditor.ViewModel
{
	/// <summary>
	/// Класс команды.
	/// </summary>
    public class RelayCommand : ICommand
    {

        #region Properties
		/// <summary>
		/// Действие, которое должно быть выполнено.
		/// </summary>
        private readonly Action<object> ExecuteAction;
		/// <summary>
		/// Определяет, может ли быть выполнена команда.
		/// </summary>
		private readonly Predicate<object> CanExecuteAction;

        #endregion

		/// <summary>
		/// Конструктор.
		/// </summary>
		/// <param name="action">Действие, которое должно быть выполнено.</param>
		/// <param name="canExecute">Метод, позволяющий определить, может ли быть выполнена команда.</param>
        public RelayCommand(Action<object> action, Predicate<object> canExecute)
        {
            ExecuteAction = action;
            CanExecuteAction = canExecute;
        }

		/// <summary>
		/// Конструктор для всегда доступных команд.
		/// </summary>
		/// <param name="execute"></param>
        public RelayCommand(Action<object> execute)
            : this(execute, _ => true)
        {
        }
		#region Methods

		/// <summary>
		/// Метод, определяющий, может ли быть выполнена команду.
		/// </summary>
		/// <param name="parameter"></param>
		/// <returns>Можно ли выполнить команду</returns>
		public bool CanExecute(object parameter)
        {
            return CanExecuteAction(parameter);
        }

		/// <summary>
		/// Происходит при измеении доступности команды.
		/// </summary>
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

		/// <summary>
		/// Выполняет команду.
		/// </summary>
		/// <param name="parameter"></param>
        public void Execute(object parameter)
        {
            ExecuteAction(parameter);
        }

        #endregion
    }
}
