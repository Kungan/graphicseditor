﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfGraphicsEditor
{
	/// <summary>
	/// Представляет собой возможные составляющие изображения.
	/// </summary>
	public enum ImageComponents
	{
		/// <summary>
		/// Компонента яркости.
		/// </summary>
		Y,

		/// <summary>
		/// Синий цветоразностный компонент.
		/// </summary>
		Cb,

		/// <summary>
		/// Красный цветоразностный компонент.
		/// </summary>
		Cr,

		/// <summary>
		/// Красный канал.
		/// </summary>
		Red,

		/// <summary>
		/// Зеленый канал.
		/// </summary>
		Green,

		/// <summary>
		/// Синий канал.
		/// </summary>
		Blue
	}
}
